# Deploy #

## Installation ##


1. Поместите файлы проекта в директорию `/var/www/html/coords_back`. 

    Если расположить проект в другой директории, дальнейшая инструкция должна быть 
    изменена в соответствии с измененным путем до проекта
1. Установите python 3 (проект проверялся на версиях питона 3.5 и 3.7)
2. Установите зависимости: `pip3 install -r requirements.txt`
3. Скопируйте  `config.example.yaml` как локальный конфиг (например, `config.yaml`)

## Before usage ##

#### В базе s11 оздайте таблицы с помощью команд: ####

1. Создайте таблицу `Geolocation`:
    ```SQL
    CREATE TABLE IF NOT EXISTS Geolocation(
      id INT AUTO_INCREMENT NOT NULL,
      lat FLOAT NOT NULL COMMENT 'Широта',
      `long` FLOAT NOT NULL COMMENT 'Долгота',
      createDate DATETIME DEFAULT NULL 'Дата и время создания',
      person_id INT NOT NULL 'Идентификатор врача, которому принадлежит точка',
      PRIMARY KEY (id),
      FOREIGN KEY (person_id) REFERENCES Person(id)
    ) COMMENT 'Геоданные врачей';
    ```

2. Создайте таблицу `HomeRequestType`:
    ```sql
    CREATE TABLE HomeRequestType (
      id INT NOT NULL AUTO_INCREMENT,
      code varchar(16) NOT NULL COMMENT 'Код',
      name varchar(64) NOT NULL COMMENT 'Имя типа заявки',
      PRIMARY KEY (id)
    ) COMMENT 'Тип заявки вызова врача на дом';
    ```

3. Обновите таблицу `HomeRequestType`:
    ```sql
    INSERT INTO HomeRequestType(code, name)
    VALUES
           ('wait', 'Ждет обработки'),
           ('processed', 'Обработано'),
           ('canceled', 'Отменено');
    ```

4. Создайте таблицу `HomeRequest`:
    ```sql
    CREATE TABLE IF NOT EXISTS HomeRequest(
      id INT AUTO_INCREMENT NOT NULL,
      create_date DATE COMMENT 'Дата создания',
      create_time TIME COMMENT 'Время создания',
      claim TEXT COMMENT 'Причина, по которой была создана заявка',
      client_id INT NOT NULL COMMENT 'Клиент, которому оформили заявку',
      type_id INT NOT NULL COMMENT 'Тип заявки',
      action_id INT COMMENT 'Действие типа очередь (queue)',
      reason_of_cancel TEXT COMMENT 'Причина отмены заявки',
      operator_id INT COMMENT 'Оператор, который оформлял заявку',
      org_structure_id INT COMMENT 'Номер отделения, к которому привязан HomeRequest (OrgStructure)',
      note TEXT DEFAULT '' COMMENT 'Примечание к заявке'
      PRIMARY KEY (id),
      FOREIGN KEY (client_id) REFERENCES Client(id),
      FOREIGN KEY (type_id) REFERENCES HomeRequestType(id),
      FOREIGN KEY (action_id) REFERENCES Action(id),
      FOREIGN KEY (operator_id) REFERENCES Person(id)
      FOREIGN KEY (org_structure_id) REFERENCES OrgStructure(id)
    ) COMMENT 'Заявки для вызова врача на дом';
    ```
5. Создайте таблицу `HomeEventFile`:
    ```sql
    CREATE TABLE file.HomeEventFile (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `createDatetime` datetime NOT NULL COMMENT 'Дата создания записи',
      `event_id` int(11) NOT NULL COMMENT 'Событие {Event}',
      `name` varchar(80) NOT NULL COMMENT 'Название файла',
      `file` mediumblob NOT NULL COMMENT 'Содержимое файла',
      `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Отметка удаления записи',
      PRIMARY KEY (`id`),
      FOREIGN KEY (`event_id`) REFERENCES s11.Event(id)
    ) COMMENT='Файлы, относящиеся к событию/осмотру';
    ```
## Usage ##

#### Чтобы запустить API как сервис: ####

1. В директории `/etc/systemd/system` создайте файл `coords_back.service`
2. Добавьте в файл: 
    ```
    [Unit]
    Description=Coords back service
    
    [Service]
    Restart=always
    RestartSec=10
    Environment=COORDS_CONFIG=PATH_TO_CONFIG
    ExecStart=PYTHON_PATH MAIN_PATH
    WorkingDirectory=PROJECT_FOLDER
    
    [Install]
    WantedBy=multi-user.target
    ```
    Где:
     * PATH_TO_CONFIG - Полный путь до конфига. Example - `/var/www/html/coords_back/config.yaml`
     * PYTHON_PATH - Полный путь до интерпретатора python3. Example - `/var/www/html/coords_back/venv/bin/python3`
     * MAIN_PATH - Полный путь до файла main.py в папке проекта. Example - `/var/www/html/coords_back/coords_back/main.py`
     * PROJECT_FOLDER - Полный путь до проекта. Example - `/var/www/html/coords_back`
3. Активируйте сервис командой `systemctl enable coords_back`
4. Запуск, остановка или перезапуск сервиса с помощью команд `service coords_back start|stop|restart`
