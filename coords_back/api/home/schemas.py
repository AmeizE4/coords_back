from datetime import datetime

from marshmallow import Schema, fields, validates, ValidationError, post_load

from database import execute_query


class AppointmentHomePostSchema(Schema):
    homeRequestId = fields.Int(
        required=True,
        attribute='home_request_id'
    )
    date = fields.Str(
        required=True,
        format='%Y-%m-%d'
    )
    doctorId = fields.Int(
        required=True,
        attribute='doctor_id'
    )

    @validates('homeRequestId')
    def validate_hr(self, id):
        hr = execute_query("select id from HomeRequest WHERE id=%(id)s",
                           {'id': id},
                           first=True)
        if not hr:
            raise ValidationError('no such request')

    @validates('doctorId')
    def validate_person(self, person_id):
        doctor = execute_query("select id from Person WHERE id=%(id)s",
                               {'id': person_id},
                               first=True) or {}
        if not doctor:
            raise ValidationError('no such person')

    @post_load()
    def return_home_request(self, data):
        data['home_request'] = execute_query(
            """
            SELECT HR.id, HRT.code, HR.action_id 
            FROM HomeRequest HR
            LEFT JOIN HomeRequestType HRT on HR.type_id = HRT.id
            WHERE HR.id = %(home_request_id)s
            """,
            {"home_request_id": data['home_request_id']},
            first=True
        )
        return data


class AppointmentHomeDeleteSchema(Schema):
    homeRequestId = fields.Int(required=True, attribute='home_request_id')
    reason = fields.Str(required=True)

    @validates('homeRequestId')
    def validate_hr(self, id):
        hr = execute_query("select id from HomeRequest WHERE id=%(id)s",
                           {'id': id},
                           first=True)
        if not hr:
            raise ValidationError('no such request')

    @post_load()
    def return_data_to_delete(self, data):
        data['home_request'] = execute_query(
            """
            SELECT HRT.code AS code, HR.id as id
            FROM HomeRequest HR
                LEFT JOIN HomeRequestType HRT on HR.type_id = HRT.id
            WHERE HR.id = %(home_request_id)s""",
            {"home_request_id": data.pop('home_request_id')},
            first=True
        )
        return data


class HomeRequestGetSchema(Schema):
    date = fields.Date(
        required=True,
        format='%Y-%m-%d'
    )


class HomeRequestPostPutSchema(Schema):
    id = fields.Int(required=True)
    clientId = fields.Int(
        required=True,
        attribute='client_id'
    )
    operatorId = fields.Int(
        required=True,
        attribute='operator_id'
    )
    orgStructureId = fields.Int(
        required=True,
        attribute='org_structure_id'
    )
    claim = fields.Str()
    note = fields.Str()

    @validates('id')
    def validate_home_request(self, hr_id):
        hr_id = execute_query("select id from HomeRequest WHERE id=%(id)s",
                              {'id': hr_id},
                              first=True)
        if not hr_id:
            raise ValidationError('no such home request')

    @validates('clientId')
    def validate_client(self, person_id):
        client = execute_query("select id from Client WHERE id=%(id)s",
                               {'id': person_id},
                               first=True)
        if not client:
            raise ValidationError('no such client')

    @validates('operatorId')
    def validate_person(self, person_id):
        person = execute_query("select id from Person WHERE id=%(id)s",
                               {'id': person_id},
                               first=True)
        if not person:
            raise ValidationError('no such person')

    @validates('orgStructureId')
    def validate_department(self, org_structure_id):
        org_structure = execute_query(
            """
            SELECT id from OrgStructure 
            WHERE id=%(id)s 
                  AND parent_id IS NULL
            """,
            {'id': org_structure_id},
            first=True
        )
        if not org_structure:
            raise ValidationError('no such main org structure')

    @post_load()
    def return_home_request_record(self, data):
        if not data.get('id'):
            # if not id - load for post method
            data['create_date'] = datetime.now().strftime('%Y-%m-%d')
            data['create_time'] = datetime.now().strftime('%H:%M:%S')
            data['type_id'] = execute_query(
                "SELECT id FROM HomeRequestType WHERE code = 'wait'",
                first=True
            )['id']
            return data
        elif data.get('id'):
            # if get id - load for put method
            if len(data) <= 1:
                # if put method don't get any arguments to change - raise error
                raise ValidationError("Must include at least one field to change")
            return data
        else:
            raise ValidationError
