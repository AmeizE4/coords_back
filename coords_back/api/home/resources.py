from datetime import datetime

from flask import request, make_response, jsonify
from flask_restful import Resource

import const
from database import execute_query, get_db
from .schemas import AppointmentHomePostSchema, AppointmentHomeDeleteSchema, HomeRequestGetSchema, \
    HomeRequestPostPutSchema
from utils import list_to_sql_array, list_to_sql_array_with_wrap, list_to_sql_array_with_equal_wrap
from api.time_table.resources import get_action_id, _get_current_person, set_action_property


class AppointmentHomeResource(Resource):
    schema = AppointmentHomePostSchema()

    def post(self):
        # language=yaml
        """
        Оформить заявку
        ---
        tags:
        - home
        description: >
          Оформление заявки врача на дом
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              homeRequestId:
                type: number
              date:
                type: string
                example: "YYYY-mm-dd"
              doctorId:
                type: number
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data
        date = data.get('date')
        doctor_id = data.get('doctor_id')
        home_request = data['home_request']
        person_info = execute_query(
            """
            select p.org_id AS orgId, p.orgStructure_id, s.sex, s.age 
            from Person p 
                left join rbSpeciality s on s.id = p.speciality_id 
            where p.id=%(person_id)s
            """,
            {"person_id": doctor_id},
            first=True
        )
        client = execute_query(
            """SELECT client_id AS id FROM HomeRequest WHERE id = %(home_request_id)s""",
            {"home_request_id": home_request['id']},
            first=True
        )

        if home_request['code'] == 'processed':
            return make_response(jsonify(
                error='Данная заявка уже обработана'
            ), 400)

        req_cnt = execute_query(
            const.select_req_cnt_home,
            {
                "id_pat": client['id'],
                "person_id": doctor_id,
                "set_date": datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            },
            first=True
        )
        if req_cnt['cnt']:
            # Уже есть запись к врачу этой специальности
            return make_response(jsonify(
                error='Пациент имеет предстоящую запись к данному врачу/врачу этой специальности'
            ), 400)

        action_id = get_action_id('home', doctor_id, date)
        if not action_id:
            return make_response(jsonify(
                error="Указан недопустимый идентификатор врача или неверная дата"
            ), 400)

        # Если у врача нет больше индексов в Action_home - к нему нельзя записаться.
        home_action = execute_query(
            (const.select_appointments_home_without_value
             + "AND p.id = %(doctor_id)s"),
            {
                "search_date": date,
                "doctor_id": doctor_id
            },
            first=True
        )
        if not home_action:
            return make_response(jsonify(
                error='У врача закончились индексы на запись'
            ), 400)

        get_db().begin()
        try:
            date_time = "{0} {1}".format(date, "00:00:00")
            event_type_id = execute_query(
                """SELECT id FROM EventType where code='queue'""",
                first=True
            )['id']
            action_type_id = execute_query(
                """SELECT id FROM ActionType where code='queue'""",
                first=True
            )['id']
            now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            event_record = {
                'createDatetime': now,
                'createPerson_id': _get_current_person(),
                'eventType_id': event_type_id,
                'org_id': person_info['orgId'],
                'client_id': client['id'],
                'setDate': date_time,
                'isPrimary': 1,
                # None insert
                'modifyDatetime': now,
                'externalId': "",
                '`order`': 0,
                'payStatus': 0,
                'note': "",
                'totalCost': 0,
            }
            event_id = execute_query(
                """
                insert into Event({0}) 
                values({1})
                """.format(
                    list_to_sql_array(event_record),
                    list_to_sql_array_with_wrap(event_record)
                ),
                event_record
            )

            action_record = {
                'createDatetime': now,
                'createPerson_id': _get_current_person(),
                'actionType_id': action_type_id,
                'event_id': event_id,
                'directionDate': date_time,
                'status': 1,
                'person_id': doctor_id,
                'setPerson_id': _get_current_person(),
                'note': "Записано через РФ ЕГИСЗ",
                'office': '',
                # None insert
                'modifyDatetime': now,
                'plannedEndDate': now,
                'amount': 0,
                'payStatus': 0,
                'account': 0,
                'MKB': "",
                'morphologyMKB': "",
                'coordText': "",
            }
            queue_action_id = execute_query(
                """
                insert into Action({0}) 
                values({1})
                """.format(
                    list_to_sql_array(action_record),
                    list_to_sql_array_with_wrap(action_record),
                ),
                action_record
            )

            answer = set_action_property(
                action_id,
                'queue',
                'ActionProperty_Action',
                home_action['ticketIndex'],
                queue_action_id
            )

            execute_query(
                """
                UPDATE HomeRequest 
                SET type_id = %(new_type_id)s, 
                    action_id = %(queue_action_id)s
                WHERE HomeRequest.id = %(home_request_id)s
                """,
                {
                    "new_type_id": execute_query(
                        """SELECT id FROM HomeRequestType HRT WHERE HRT.code = 'processed'""",
                        first=True
                    )['id'],
                    "queue_action_id": queue_action_id,
                    "home_request_id": home_request['id'],
                },
            )
            get_db().commit()
        except Exception as e:
            get_db().rollback()
            raise e

        if answer and answer.get('type') == 'error':
            return answer
        return make_response(jsonify(info="OK"), 200)

    schema_del = AppointmentHomeDeleteSchema()

    def delete(self):
        # language=yaml
        """
        Отмена заявки
        ---
        tags:
        - home
        description: >
          Отменяет заявку, освобождает график врача (если заявка была
          оформлена), статус заявки меняется на "Отменено"
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              homeRequestId:
                type: number
              reason:
                type: string
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """
        marsh_result = self.schema_del.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        data = marsh_result.data
        reason = data['reason']
        home_request = data['home_request']

        if home_request.get('code') == 'wait':
            home_request_type = execute_query(
                """SELECT id FROM HomeRequestType WHERE code='canceled'""",
                first=True
            )
            execute_query(
                """
                UPDATE HomeRequest HR
                SET HR.type_id = %(home_request_type)s,
                    HR.reason_of_cancel = %(reason)s
                WHERE HR.id = %(home_request_id)s
                """,
                {'home_request_type': home_request_type['id'],
                 'reason': reason,
                 'home_request_id': home_request['id']}
            )
            return make_response(jsonify(info='OK'), 200)
        if home_request.get('code') != 'processed':
            return make_response(jsonify(
                error='Заявка не обработана'
            ))
        to_delete = execute_query(
            """
            SELECT AP_A.id as ap_a_id, 
                   AP_A.index as `index`,
                   A.id as queue_action_id, 
                   E.id as queue_event_id
            FROM HomeRequest HR
                   INNER JOIN ActionProperty_Action AP_A ON AP_A.value = HR.action_id
                   INNER JOIN Action A ON AP_A.value = A.id
                   INNER JOIN Event E on A.event_id = E.id
            WHERE HR.id = %(home_request_id)s
            """,
            {"home_request_id": home_request['id']},
            first=True
        )

        get_db().begin()
        try:
            home_request_type = execute_query(
                """SELECT id FROM HomeRequestType WHERE code='canceled'""",
                first=True
            )
            execute_query(
                """
                UPDATE HomeRequest HR
                SET HR.action_id = NULL, 
                    HR.type_id = %(home_request_type)s,
                    HR.reason_of_cancel = %(reason)s
                WHERE HR.id = %(home_request_id)s
                """,
                {
                    'home_request_id': home_request['id'],
                    'home_request_type': home_request_type['id'],
                    'reason': reason
                }
            )
            execute_query(
                """
                UPDATE ActionProperty_Action AP_A
                SET AP_A.value = NULL
                WHERE AP_A.id = %(id)s
                    AND AP_A.index = %(index)s
                """,
                {'id': to_delete['ap_a_id'], 'index': to_delete['index']}
            )
            execute_query(
                """
                DELETE FROM Action 
                WHERE Action.id = %(queue_action_id)s
                """,
                {'queue_action_id': to_delete['queue_action_id']}
            )
            execute_query(
                """
                DELETE FROM Event
                WHERE Event.id = %(queue_event_id)s
                """,
                {'queue_event_id': to_delete['queue_event_id']}
            )
            get_db().commit()
        except Exception as e:
            get_db().rollback()
            raise e

        return make_response(jsonify(info='OK'), 200)

    def put(self):
        # language=yaml
        """
        Перенос записи
        ---
        tags:
        - home
        description: Переносит запись из одного расписания в другое
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              homeRequestId:
                type: number
              date:
                type: string
                example: "YYYY-mm-dd"
              doctorId:
                type: number
        responses:
          '200':
            description: ok
          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data
        date = data.get('date')
        doctor_id = data.get('doctor_id')
        home_request = data['home_request']

        if home_request['code'] != 'processed':
            return make_response(jsonify(
                error='Данная заявка не обработана или отменена'
            ), 400)

        action_id = get_action_id('home', doctor_id, date)
        if not action_id:
            return make_response(jsonify(
                error="Указан недопустимый идентификатор врача или неверная дата"
            ), 400)

        # Если у врача нет больше индексов в Action_home - к нему нельзя записаться.
        home_action = execute_query(
            (const.select_appointments_home_without_value
             + "AND p.id = %(doctor_id)s"),
            {
                "search_date": date,
                "doctor_id": doctor_id
            },
            first=True
        )
        if not home_action:
            return make_response(jsonify(
                error='У врача закончились индексы на запись'
            ), 400)

        get_db().begin()
        try:
            execute_query(
                """
                UPDATE ActionProperty_Action APA
                SET APA.value = NULL
                WHERE APA.value = %(value)s
                """,
                {"value": home_request['action_id']}
            )
            set_action_property(
                action_id,
                'queue',
                'ActionProperty_Action',
                home_action['ticketIndex'],
                home_request['action_id']
            )
            execute_query(
                """
                UPDATE Action
                SET person_id = %(person_id)s
                WHERE id = %(action_id)s
                """,
                {
                    "person_id": doctor_id,
                    "action_id": home_request['action_id']
                }
            )
            event = execute_query(
                """
                SELECT E.id 
                FROM Event E 
                    LEFT JOIN Action A on A.event_id = E.id
                WHERE A.id = %(id)s
                """,
                {"id": home_request['action_id']},
                first=True
            )
            execute_query(
                """
                UPDATE Event
                SET Event.execPerson_id = %(person_id)s
                WHERE id = %(event_id)s
                """,
                {
                    "person_id": doctor_id,
                    "event_id": event['id']
                }
            )
            get_db().commit()
        except Exception as e:
            get_db().rollback()
            raise e

        return make_response(jsonify(info="OK"), 200)


class HomeRequestResource(Resource):
    schema_post = HomeRequestPostPutSchema(only=('clientId', 'operatorId',
                                                 'orgStructureId', 'claim', 'note'))

    def post(self):
        # language=yaml
        """
        Оставить заявку на вызов врача на дом
        ---
        tags:
        - home
        description:
        parameters:
          - in: body
            name: body
            description: >
              Параметры записи вызова врача на дом.

              Допустимые значения для orgStructureId можно получить из метода
              MainDepartments.
            schema:
              type: object
              properties:
                clientId:
                  type: number
                operatorId:
                  type: number
                claim:
                  type: string
                note:
                  type: string
                orgStructureId:
                  type: number
        responses:
          '201':
            description: Запрос успешен
            schema:
              type: object
              properties:
                id:
                  type: number
          '400':
            description: Bad Request

        """
        marsh_result = self.schema_post.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)

        home_request_record = marsh_result.data

        home_request_id = execute_query(
            """
            INSERT INTO HomeRequest({}) VALUES({})
            """.format(
                list_to_sql_array(home_request_record),
                list_to_sql_array_with_wrap(home_request_record),
            ),
            home_request_record
        )
        return make_response(jsonify(id=home_request_id), 201)

    schema_get = HomeRequestGetSchema()

    def get(self):
        # language=yaml
        """
        Заявки вызова врача на дом
        ---
        tags:
        - home
        description: Возвращает все заявки вызова врача на дом на указанный день
        parameters:
        - in: query
          name: date
          type: string
          description: Дата, на которую нужно получить заявки, формат YYYY-mm-dd
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: array
              items:
                type: object
                properties:
                  id:
                    type: number
                  time:
                    type: string
                    example: HH-MM-SS
                  claim:
                    type: string
                  reason_of_cancel:
                    type: string
                  note:
                    type: string
                  orgStructure:
                    type: object
                    properties:
                      code:
                        type: string
                      id:
                        type: number
                  type_name:
                    type: string
                  operator:
                    type: object
                    properties:
                      id:
                        type: number
                      name:
                        type: number
                  doctor:
                    type: object
                    properties:
                      id:
                        type: number
                      name:
                        type: string
                  client:
                    type: object
                    properties:
                      id:
                        type: number
                        format: int32
                      last_name:
                        type: string
                      first_name:
                        type: string
                      patr_name:
                        type: string
                      birth_date:
                        type: string
                        example: YYYY-mm-dd
                      address:
                        type: string
          '400':
            description: Bad Request
        """
        marsh_result = self.schema_get.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)

        home_requests = execute_query(
            """
            SELECT HR.id                                   as id,
                   TIME_FORMAT(HR.create_time, '%%T')      as time,
                   HR.claim                                as claim,
                   HR.reason_of_cancel,
                   HR.note,
                   OS.id                                   AS orgStructureId,
                   OS.code                                 AS orgStructureCode,
                   HRT.name                                as type_name,
                   C.id                                    as client_id,
                   C.firstName,
                   C.lastName,
                   C.patrName,
                   DATE_FORMAT(C.birthDate, '%%Y-%%m-%%d') AS birthDate,
                   getClientLocAddress(C.id)               as address,
                   Operator.id                             as operator_id,
                   Operator.lastName                       as operator_name,
                   Doctor.id                               as doctor_id,
                   Doctor.lastName                         as doctor_name
            FROM HomeRequest HR
              LEFT JOIN Client C on HR.client_id = C.id
              LEFT JOIN HomeRequestType HRT on HR.type_id = HRT.id
              LEFT JOIN Person Operator on HR.operator_id = Operator.id
              LEFT JOIN Action A on HR.action_id = A.id
              LEFT JOIN Person Doctor on A.person_id = Doctor.id
              LEFT JOIN OrgStructure OS ON HR.org_structure_id = OS.id
            WHERE HR.create_date = %(date)s
            """,
            marsh_result.data
        )
        answer = []
        for elem in home_requests:
            answer.append({
                "id": elem['id'],
                "time": elem['time'],
                "claim": elem['claim'],
                "reasonOfCancel": elem['reason_of_cancel'],
                "note": elem['note'],
                "orgStructure": {
                    'id': elem['orgStructureId'],
                    'code': elem['orgStructureCode'],
                },
                "typeName": elem['type_name'],
                "operator": {
                    "id": elem['operator_id'],
                    "name": elem['operator_name'],
                },
                "client": {
                    "id": elem['client_id'],
                    "firstName": elem['firstName'],
                    "lastName": elem['lastName'],
                    "patrName": elem['patrName'],
                    "birthDate": elem['birthDate'],
                    'address': elem['address']
                },
                "doctor": {
                    "id": elem['doctor_id'],
                    "name": elem['doctor_name']
                }
            })
        return make_response(jsonify(answer))

    schema_put = HomeRequestPostPutSchema(only=('id', 'claim',
                                                'note', 'orgStructureId'))

    def put(self):
        # language=yaml
        """
        Изменение HomeRequest
        ---
        tags:
        - home
        description: >
          Позволяет изменять поля в HomeRequest.

          Дополнительные поля будут игнорироваться.
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              id:
                type: number
              orgStructureId:
                type: number
              claim:
                type: string
              note:
                type: string
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """
        marsh_result = self.schema_put.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        home_request_record = marsh_result.data
        home_request_id = home_request_record.pop('id')

        execute_query(
            """
            UPDATE HomeRequest
            SET {0}
            WHERE id = %(id)s
            """.format(list_to_sql_array_with_equal_wrap(home_request_record)),
            dict({'id': home_request_id}, **home_request_record)
        )

        return make_response(jsonify(info='OK'), 200)


class MainDepartments(Resource):

    def get(self):
        # language=yaml
        """
        Основные отделения
        ---
        tags:
        - home
        description: Возвращает основные (родительские) отделения поликлиники
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: object
              properties:
                id:
                  type: number
                code:
                  type: string
        """
        org_structs = execute_query(
            """
            SELECT os.id, os.code
            FROM OrgStructure os
            WHERE os.parent_id IS NULL
            AND deleted = 0
            """
        )
        return org_structs
