from datetime import datetime

from marshmallow import Schema, fields, pre_load, validates, ValidationError

from database import execute_query


class JobTicketGetSchema(Schema):
    searchDate = fields.Date(format='%Y-%m-%d', attribute='search_date')
    actionTypesIds = fields.List(
        fields.Int(),
        attribute='action_types_ids'
    )

    @pre_load
    def create_action_types(self, data):
        data = data.to_dict()
        # FIXME 500 if no actionTypes in data
        # FIXME is it right way to parse array on arguments?
        if '[' in data['actionTypesIds'] and ']' in data['actionTypesIds']:
            data['actionTypesIds'] = data['actionTypesIds'].replace('[', '').replace(']', '')
        data['actionTypesIds'] = [int(i) for i in data.get('actionTypesIds').split(',')]
        return data

    @validates('actionTypesIds')
    def validate_action_types_ids(self, at_ids):
        for i in at_ids:
            at = execute_query(
                """
                SELECT id FROM ActionType WHERE id = %(id)s
                """,
                {"id": i}
            )
            if not at:
                raise ValidationError('One of ActionTypes does not exist')


class JobTicketsPostSchema(Schema):
    eventId = fields.Int(
        required=True,
        attribute='event_id'
    )
    actionTypeIds = fields.List(
        fields.Int(),
        required=True,
        attribute='action_type_ids'
    )
    jobTicketId = fields.Int(
        required=True,
        attribute='job_ticket_id'
    )

    @validates('eventId')
    def is_event_exist(self, e_id):
        event = execute_query(
            "SELECT id FROM Event WHERE id=%(id)s",
            {'id': e_id},
            first=True
        )
        if not event:
            raise ValidationError('Event does not exist')

    @validates('actionTypeIds')
    def is_action_types_exist(self, at_ids):
        if len(at_ids) < 1:
            raise ValidationError('Must contain at least one actionType')
        for at_id in at_ids:
            action_type = execute_query(
                """SELECT id FROM ActionType WHERE id=%(at_id)s""",
                {'at_id': at_id},
                first=True
            )
            if not action_type:
                raise ValidationError('One of actions types does not exist')

    @validates('jobTicketId')
    def job_ticket_res_check(self, jt_id):
        jt = execute_query(
            "SELECT resTimestamp, DATE(datetime) as datetime FROM Job_Ticket WHERE id=%(id)s",
            {'id': jt_id},
            first=True
        )
        if not jt:
            raise ValidationError('Job Ticket does not exist')
        if jt['resTimestamp'] is not None:
            raise ValidationError('Job Ticket is already reserved')
        if jt['datetime'] < datetime.now().date():
            raise ValidationError('Cannot reserve job ticket on past date')
