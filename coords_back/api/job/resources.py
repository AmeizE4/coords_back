from datetime import datetime

from flask import make_response, jsonify, request
from flask_restful import Resource

from database import execute_query, get_db
from .schemas import JobTicketGetSchema, JobTicketsPostSchema
from utils import list_to_sql_array, list_to_sql_array_with_wrap


class TissueTypesResource(Resource):
    def get(self):
        # language=yaml
        """
        Типы анализов
        ---
        tags:
        - job
        description: >
          Возвращает типы анализов, на которые можно произвести запись
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: array
              items:
                type: object
                properties:
                  group_name:
                    type: string
                  types:
                    type: array
                    items:
                      type: object
                      properties:
                        name:
                          type: string
                        id:
                          type: number
                        rbJobTicket:
                          type: object
                          properties:
                            id:
                              type: number
                            name:
                              type: string
        """
        tissue_types = execute_query(
            """
            SELECT A.name,
                   A.id,
                   DT.id        AS group_id,
                   DT.name      AS group_name,
                   rbJT.id      AS rbJobTicketId,
                   rbJT.name    AS rbJobTicketName
            FROM DestinationTree DT
                   INNER JOIN DestinationTree_ActionType DT_AT ON DT.id = DT_AT.master_id
                   INNER JOIN ActionType A ON A.id = DT_AT.actionType_id
            
                   INNER JOIN ActionPropertyType APT ON A.id = APT.actionType_id AND APT.typeName = 'JobTicket'
                   INNER JOIN rbJobType rbJT ON APT.valueDomain = rbJT.code
            WHERE DT.showInMobileApp = 1
            """
        )
        answer = {
            tt['group_id']: {
                'group_name': tt['group_name'],
                'types': []
            } for tt in tissue_types
        }
        for tt in tissue_types:
            answer[tt['group_id']]['types'].append({
                'name': tt['name'],
                'id': tt['id'],
                'rbJobTicket': {
                    "id": tt['rbJobTicketId'],
                    "name": tt['rbJobTicketName'],
                }
            })
        return make_response(jsonify(list(answer.values())))


class JobTicketsResource(Resource):
    schema_get = JobTicketGetSchema()

    def get(self):
        # language=yaml
        """
        Тикеты для конкретных анализов
        ---
        tags:
        - job
        description: >
          Возвращает тикеты, организованные по структурам.
        parameters:
        - in: query
          name: actionTypesIds
          type: array
          items:
            type: number
        - in: query
          name: searchDate
          type: string
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: object
              properties:
                rbJobTickets:
                  type: array
                  items:
                    type: object
                    properties:
                      name:
                        type: string
                      id:
                        type: number
                      orgStructures:
                        type: array
                        items:
                          type: object
                          properties:
                            id:
                              type: number
                            name:
                              type: string
                            jobTickets:
                              type: array
                              items:
                                type: object
                                properties:
                                  id:
                                    type: number
                                  status:
                                    type: number
                                  index:
                                    type: number
                                  time:
                                    type: string
        """
        marsh_result = self.schema_get.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        tickets = execute_query(
            """
            SELECT ActionType.id AS ActionTypeId, 
                   ActionType.name AS ActionTypeName,
                   JT.id AS jobTicketId,
                   JT.status,
                   JT.idx AS JobTicketIndex, 
                   DATE_FORMAT(JT.datetime, '%%T') AS JobTicketTime, 
                   rbJT.id AS rbJobTicketId,
                   rbJT.name AS rbJobTicketName,
                   OS.id AS os_id,
                   OS.name AS os_name
            FROM ActionType
               INNER JOIN ActionPropertyType APT ON ActionType.id = APT.actionType_id AND APT.typeName='JobTicket'
               INNER JOIN rbJobType rbJT ON APT.valueDomain = rbJT.code
             
               INNER JOIN Job Job ON Job.jobType_id = rbJT.id
               INNER JOIN OrgStructure_Job OSJ on Job.orgStructureJob_id = OSJ.id
               INNER JOIN OrgStructure OS ON OS.id = OSJ.master_id
             
               INNER JOIN Job_Ticket JT on Job.id = JT.master_id AND JT.resTimestamp IS NULL
             WHERE DATE(JT.datetime) = DATE(%(search_date)s)
                   AND ActionType.id IN %(action_types_ids)s
             GROUP BY JT.id
             ORDER BY JT.datetime, JT.idx;
            """,
            marsh_result.data
        )
        # yes it looks like a shit
        # but idk how to make it looking better without more sql queries
        rb_job_tickets = {
            t['rbJobTicketId']: {
                'id': t['rbJobTicketId'],
                'name': t['rbJobTicketName'],
                'orgStructures': {
                    tt['os_id']: {
                        'id': tt['os_id'],
                        'name': tt['os_name'],
                        'jobTickets': []
                    } for tt in tickets if tt['rbJobTicketId'] == t['rbJobTicketId']
                }
            } for t in tickets
        }
        for t in tickets:
            rb_job_tickets[t['rbJobTicketId']]['orgStructures'][t['os_id']]['jobTickets'].append({
                'id': t['jobTicketId'],
                'status': t['status'],
                'index': t['JobTicketIndex'],
                'time': t['JobTicketTime'],
            })
        for a in rb_job_tickets.values():
            a['orgStructures'] = list(a['orgStructures'].values())
        return make_response(jsonify({'rbJobTickets': list(rb_job_tickets.values())}))

    schema_post = JobTicketsPostSchema()

    def post(self):
        # language=yaml
        """
        Создание записи на анализы
        ---
        tags:
        - job
        description: >
          Делает запись на анализы, резервирует тикет, на который производится
          запись, если отправляется несколько ActionTypes - запись каждого из
          них делается на один тикет.
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              eventId:
                type: number
              jobTicketId:
                type: number
              actionTypeIds:
                type: array
                items:
                  type: number
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """

        marsh_result = self.schema_post.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data
        event_id = data.get('event_id')
        action_type_ids = data.get('action_type_ids')
        job_ticket_id = data.get('job_ticket_id')

        now = datetime.now()
        person = execute_query(
            """
            SELECT P.id
            FROM Person P
                INNER JOIN Event ON Event.createPerson_id = P.id
            WHERE Event.id = %(event_id)s
            """,
            {"event_id": event_id},
            first=True
        )
        execute_query(
            """
            UPDATE Job_Ticket J_T
            SET J_T.resTimestamp=NOW(), 
                J_T.resConnectionId=CONNECTION_ID(), 
                J_T.person_id=%(person_id)s
            WHERE J_T.id = %(job_ticket_id)s
            """,
            {
                "person_id": person['id'],
                "job_ticket_id": job_ticket_id,
            }
        )

        get_db().begin()
        try:
            for action_type_id in action_type_ids:
                action_property_type = execute_query(
                    "SELECT id FROM ActionPropertyType WHERE actionType_id=%(action_type_id)s AND typeName='JobTicket'",
                    {"action_type_id": action_type_id},
                    first=True
                )
                action_record = {
                    'createDatetime': now,
                    'createPerson_id': person['id'],
                    'actionType_id': action_type_id,
                    'event_id': event_id,
                    'directionDate': now,
                    'status': 1,
                    'person_id': person['id'],
                    'setPerson_id': person['id'],
                    'note': "Записано через приложение 'врача на дому'",
                    # None insert
                    'modifyDatetime': now,
                    'plannedEndDate': now,
                    'amount': 0,
                    'payStatus': 0,
                    'account': 0,
                    'MKB': "",
                    'morphologyMKB': "",
                    'coordText': "",
                }
                action_id = execute_query(
                    """
                    INSERT INTO Action({0})
                    VALUES({1})
                    """.format(
                        list_to_sql_array(action_record),
                        list_to_sql_array_with_wrap(action_record),
                    ),
                    action_record
                )
                # raise ValueError
                action_property_record = {
                    'createDatetime': now,
                    'createPerson_id': person['id'],
                    'action_id': action_id,
                    'type_id': action_property_type['id'],
                    # None insert
                    'modifyDatetime': now,
                }
                action_property_id = execute_query(
                    """
                    INSERT INTO ActionProperty({0})
                    VALUES({1})
                    """.format(
                        list_to_sql_array(action_property_record),
                        list_to_sql_array_with_wrap(action_property_record),
                    ),
                    action_property_record
                )
                action_property_job_ticket_record = {
                    "id": action_property_id,
                    "`index`": 0,
                    "value": job_ticket_id
                }
                action_property_job_ticket_id = execute_query(
                    """
                    INSERT INTO ActionProperty_Job_Ticket({0})
                    VALUES({1})
                    """.format(
                        list_to_sql_array(action_property_job_ticket_record),
                        list_to_sql_array_with_wrap(action_property_job_ticket_record),
                    ),
                    action_property_job_ticket_record
                )
                get_db().commit()
        except Exception as e:
            get_db().rollback()
            execute_query(
                """
                UPDATE Job_Ticket J_T
                SET J_T.resTimestamp=NULL, 
                    J_T.resConnectionId=NULL, 
                    J_T.person_id=NULL
                WHERE J_T.id = %(job_ticket_id)s
                """,
                {"job_ticket_id": job_ticket_id}
            )
            raise e
        return make_response(jsonify(info='ok'), 200)
