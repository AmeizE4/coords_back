from api.rb import base
from api.rb.PersonOrgStructure import PersonOrgStructure
from api.rb.ReasonOfAbsence import ReasonOfAbsence
from api.rb.ReceptionType import ReceptionType
from api.rb.ScheduleType import ScheduleType
from api.rb.TimeQuotingType import TimeQuotingType

RESOURCE_LIST = [
    (PersonOrgStructure, '/rb/person_OrgStructure'),
    (ReasonOfAbsence, '/rb/rbReasonOfAbsence'),
    (ReceptionType, '/rb/rbReceptionType'),
    (ScheduleType, '/rb/rbScheduleType'),
    (TimeQuotingType, '/rb/rbTimeQuotingType'),
]
