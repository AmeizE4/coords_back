from flask import request, make_response, jsonify

from api.rb import base
from database import execute_query
from api.rb.base import schemas


# TODO: soltanoff: дропнуть костыли с `cond`
class PersonOrgStructure(base.BaseRefBook):
    TABLE_NAME = 'OrgStructure'

    schema = schemas.RefBookDataGetByIDSchema()

    def get(self):
        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data

        if data:
            results = execute_query(
                self.SELECT_BY_CONDITION_QUERY.format(
                    table=self.TABLE_NAME,
                    columns=', '.join(self.COLUMNS),
                    cond='id = (SELECT orgStructure_id FROM Person WHERE id = %s)' % data['person_id']
                ),
                params=None,
                first=True
            )
        else:
            results = execute_query(
                self.SELECT_ALL_QUERY.format(
                    table=self.TABLE_NAME,
                    columns=', '.join(self.COLUMNS),
                ),
                params=None
            )

        return results