from marshmallow import Schema, fields


class RefBookBaseSchema(Schema):
    id = fields.Int(
        required=False,
        attribute='id'
    )

    code = fields.Raw(
        required=False,
        attribute='code'
    )

    name = fields.Raw(
        required=False,
        attribute='name'
    )


class RefBookDataGetByIDSchema(Schema):
    person_id = fields.Int(
        required=False,
        attribute='person_id'
    )

