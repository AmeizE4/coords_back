from flask import request, make_response, jsonify
from flask_restful import Resource

from api.rb.base import schemas
from database import execute_query


class BaseRefBook(Resource):
    INSERT_QUERY = "INSERT INTO {table} ({columns}) VALUES ({values})"
    SELECT_ALL_QUERY = "SELECT {columns} FROM {table}"
    SELECT_BY_CONDITION_QUERY = "SELECT {columns} FROM {table} WHERE {cond}"

    TABLE_NAME = None

    COLUMNS = ('id', 'code', 'name')

    schema = schemas.RefBookBaseSchema()

    def get(self):
        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data

        if data:
            results = execute_query(
                self.SELECT_BY_CONDITION_QUERY.format(
                    table=self.TABLE_NAME,
                    columns=', '.join(self.COLUMNS),
                    cond=" AND ".join(map(lambda x: "%s LIKE '%%%s%%'" % x, data.items()))
                ),
                params=None,
                first='id' in data
            )
        else:
            results = execute_query(
                self.SELECT_ALL_QUERY.format(
                    table=self.TABLE_NAME,
                    columns=', '.join(self.COLUMNS),
                ),
                params=None
            )

        return results
