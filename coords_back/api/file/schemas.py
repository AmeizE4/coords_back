from marshmallow import Schema, fields, validates, ValidationError

from database import execute_query


class FileGetSchema(Schema):
    id = fields.Int(required=True, attribute='id')

    @validates('id')
    def validate_file_id(self, file_id):
        file = execute_query(
            "SELECT id FROM file.HomeEventFile WHERE id=%(file_id)s",
            {"file_id": file_id}
        )
        if not file:
            raise ValidationError('no such file')


class FileByEventSchema(Schema):
    eventId = fields.Int(required=True, attribute='event_id')

    @validates('eventId')
    def validate_event_id(self, event_id):
        event = execute_query(
            "SELECT id FROM Event WHERE id=%(event_id)s",
            {"event_id": event_id}
        )
        if not event:
            raise ValidationError('no such event')
