from datetime import datetime
from io import BytesIO

from flask import request, make_response, jsonify, send_file
from flask_restful import Resource

from database import execute_query, get_db
from .schemas import FileGetSchema, FileByEventSchema
from utils import list_to_sql_array, list_to_sql_array_with_wrap


class File(Resource):

    def post(self):
        # language=yaml
        """
        Сохранение файлов
        ---
        tags:
        - files
        description: Метод для сохранения файлов
        consumes:
        - multipart/form-data
        parameters:
        - name: eventId
          in: formData
          description: Событие, к которому привязан файл
          type: number
        - name: AttachedFile1
          in: formData
          description: >
            Файл, который нужно сохранить. Под разными индексами принимает
            произвольное количество файлов.

            На данный момент ключ не имеет значения - принимает файлы по любому
            ключу, слова выше про индекс - планируемая фантазия, но не
            реальность.
          type: file
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """
        # TODO create validation for filenames and for file extension
        file_list = [request.files[key] for key in request.files.keys()]
        form = request.form
        event_id = form.get('eventId')
        if not all(file_list) or not event_id:
            return make_response(jsonify(error='Bad Request'), 400)

        get_db().begin()
        try:
            for f in file_list:
                file_record = {
                    "createDatetime": datetime.now(),
                    "event_id": event_id,
                    "name": f.filename,
                    "file": f.stream.read()
                }
                execute_query(
                    """
                    INSERT INTO file.HomeEventFile({0}) VALUES({1})
                    """.format(
                        list_to_sql_array(file_record),
                        list_to_sql_array_with_wrap(file_record)
                    ),
                    file_record
                )
        except Exception as e:
            get_db().rollback()
            raise e

        return make_response(jsonify(info='ok'), 200)

    schema_get = FileGetSchema()

    def get(self):
        # language=yaml
        """
        Получение файла по id
        ---
        tags:
        - files
        description: >
          Возвращает файл по id.
          Получить id файла можно с помощью метода fileByEvent.
        parameters:
        - in: query
          name: id
          description: id файла
          type: number
        responses:
          '200':
              description: Запрос успешен
              schema:
                type: string
                format: byte
                example: >
                  here must be file content, but idk how to describe it with swagger
          '400':
            description: Bad Request
        """

        marsh_result = self.schema_get.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        data = marsh_result.data

        answer = execute_query(
            """
            SELECT HEF.file, HEF.name AS filename 
            FROM file.HomeEventFile HEF 
            WHERE HEF.id=%(id)s
            """,
            data,
            first=True
        )
        return send_file(BytesIO(answer['file']), attachment_filename=answer['filename'], as_attachment=True)


class FilesByEvent(Resource):
    schema = FileByEventSchema()

    def get(self):
        # language=yaml
        """
        Получить список файлов для события
        ---
        tags:
        - files
        description: >
          Возвращает список guid'ов файлов
          Получить guid файла можно с помощью метода fileByEventId
        parameters:
        - in: query
          name: eventId
          description: event_id, к которому привязаны файлы
          type: number
        responses:
          '200':
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: number
                    filename:
                      type: string
          '400':
            description: Bad Request
        """

        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        data = marsh_result.data

        answer = execute_query(
            """
            SELECT HEF.id, HEF.name AS filename 
            FROM file.HomeEventFile HEF 
            WHERE HEF.event_id=%(event_id)s
            """,
            data
        )
        return make_response(jsonify(answer))
