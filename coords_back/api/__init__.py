from flask import Blueprint
from flask_restful import Api

from api import time_table, geo, home, file, client, job, auth, rb, schedule

api_blueprint = Blueprint('api_blueprint', __name__)
api = Api(api_blueprint)

api.add_resource(auth.RegisterResource,
                 '/auth/registration')
api.add_resource(auth.LogInResource,
                 '/auth/login')
api.add_resource(auth.SelectPersonResource,
                 '/auth/selectPerson')
api.add_resource(auth.RegisterWithBaseOrgStructureResource,
                 '/auth/registrationWithBaseOrgStructure')

api.add_resource(client.HistoryResource,
                 '/history')
api.add_resource(client.HistoryVisitsResource,
                 '/history/visits')
api.add_resource(client.ClientJobTicketResource,
                 '/client/jobTickets')

api.add_resource(geo.UserResource,
                 '/users/<person_id>')
api.add_resource(geo.GeoDoctorsResource,
                 '/geo/doctors')
api.add_resource(geo.PointResource,
                 '/point')
api.add_resource(home.AppointmentHomeResource,
                 '/homeRequest/appointment')
api.add_resource(home.HomeRequestResource,
                 '/homeRequest')
api.add_resource(home.MainDepartments,
                 '/mainDepartments')

api.add_resource(job.TissueTypesResource,
                 '/tissueTypes')
api.add_resource(job.JobTicketsResource,
                 '/jobTickets')

api.add_resource(time_table.DoctorsResource,
                 '/doctors')
api.add_resource(time_table.TimeTableResource,
                 '/timeTable')
api.add_resource(time_table.AppointmentResource,
                 '/appointment')
api.add_resource(time_table.AppointmentsResource,
                 '/appointments')
api.add_resource(time_table.WaitingList,
                 '/waitingList')
api.add_resource(time_table.QueueMove,
                 '/queueMove')

api.add_resource(file.File,
                 '/file')
api.add_resource(file.FilesByEvent,
                 '/fileByEvent')

for item in rb.RESOURCE_LIST:
    api.add_resource(*item)

for item in schedule.RESOURCE_LIST:
    api.add_resource(*item)
