from marshmallow import Schema, fields


class PersonSchema(Schema):
    person_id = fields.Int(
        required=True,
        attribute='person_id'
    )

    start_date = fields.Date(
        required=True,
        format='%Y-%m',
        attribute='start_date'
    )

