import calendar
import datetime
from collections import OrderedDict

from flask import request, make_response, jsonify
from flask_restful import Resource

from api.schedule.person import schemas
from database import execute_query


# TODO: soltanoff: нужно описать схему, добавить все необходимые проверки входящего json (см задачу VM_BACK-24)
# TODO: soltanoff: оптимизнуть execute_query: юзать генератор, получать итератор и т.д.
# TODO: soltanoff: execute_query: перейти на использование `params` для защиты от sql injections и доп. оптимизациям на стороне курсора


class ActionProperty(object):
    SELECT_ACTION_PROPERTY_TYPE = """
    SELECT 
        id, typeName, valueDomain
    FROM 
        ActionPropertyType 
    WHERE 
        actionType_id = (SELECT actionType_id FROM Action WHERE id = {action_id} AND deleted = 0)
        AND name = '{name}';
    """

    CREATE_ACTION_PROPERTY_BASE = """
    INSERT INTO 
        ActionProperty 
        (
            `createDatetime`, 
            `createPerson_id`, 
            `modifyDatetime`, 
            `modifyPerson_id`, 
            `action_id`, 
            `type_id`, 
            `norm`, 
            `isAssigned`
        ) 
    VALUES 
    (
        NOW(),                          # createDatetime
        '{person_id}',                  # createPerson_id
        NOW(),                          # modifyDatetime
        '{person_id}',                  # modifyPerson_id
        '{action_id}',                  # action_id
        '{action_property_type_id}',    # type_id
        '',                             # norm
        0                               # isAssigned
    );
    """

    CREATE_ACTION_PROPERTY_VALUE = """
    INSERT INTO 
        ActionProperty_{type_name} 
        (
            `id`, 
            `index`, 
            `value`
        ) 
    VALUES 
        (
            {action_property_id},       # id
            {index},                    # index
            {value}                   # value
        ) 
    ON DUPLICATE KEY UPDATE `value` = VALUES(`value`);\n
    """

    @classmethod
    def create_property(cls, name, action_id, person_id, values):
        action_property_type = execute_query(
            cls.SELECT_ACTION_PROPERTY_TYPE.format(
                action_id=action_id,
                name=name,
            ),
            first=True
        )
        if action_property_type is None:
            return False
        if action_property_type['typeName'] == 'Reference' and action_property_type['valueDomain'] == 'Action':
            action_property_type['typeName'] = 'Action'

        action_property_id = execute_query(
            cls.CREATE_ACTION_PROPERTY_BASE.format(
                person_id=person_id,
                action_id=action_id,
                action_property_type_id=action_property_type['id'],
            ),
        )
        for index, value in enumerate(values if isinstance(values, list) else [values]):
            execute_query(
                cls.CREATE_ACTION_PROPERTY_VALUE.format(
                    type_name=action_property_type['typeName'],
                    action_property_id=action_property_id,
                    index=index,
                    value=repr(value) if value is not None else 'NULL',
                )
            )
        return True


class Schedule(object):
    SELECT_ACTION_TYPE = """
    SELECT 
        `id`, `code`
    FROM   
        `ActionType` 
    WHERE 
        `code` = '{code}';
    """

    CREATE_EVENT_QUERY = """
    INSERT INTO 
        Event
        (
            `createDatetime`, 
            `createPerson_id`, 
            `modifyDatetime`, 
            `modifyPerson_id`, 
            `eventType_id`, 
            `setDate`, 
            `setPerson_id`, 
            `execDate`, 
            `execPerson_id`
        ) 
    VALUES 
        (
            NOW(),                                                                  # createDatetime
            '{person_id}',                                                          # createPerson_id
            NOW(),                                                                  # modifyDatetime
            '{person_id}',                                                          # modifyPerson_id
            (SELECT id FROM EventType WHERE code = '0' AND deleted = 0 LIMIT 1),    # eventType_id
            '{date}',                                                               # setDate
            '{person_id}',                                                          # setPerson_id
            '{date}',                                                               # execDate
            '{person_id}'                                                           # execPerson_id
        );
    """

    CREATE_ACTION_QUERY = """
    INSERT INTO 
        Action
        (
            `createDatetime`, 
            `createPerson_id`, 
            `modifyDatetime`, 
            `modifyPerson_id`, 
            `actionType_id`, 
            `specifiedName`, 
            `event_id`, 
            `idx`, 
            `signature`
        ) 
    VALUES 
        (
            NOW(),                  # createDatetime
            '{person_id}',          # createPerson_id
            NOW(),                  # modifyDatetime
            '{person_id}',          # modifyPerson_id
            '{action_type_id}',     # actionType_id
            '',                     # specifiedName
            '{event_id}',           # event_id
            0,                      # idx
            0                       # signature
        )
    """

    SELECT_SCHEDULE = """
    SELECT DISTINCT
        Action.id AS action_id,
        False as busy,
        DATE(Event.execDate) as date
    FROM
        Event
        INNER JOIN Action ON Event.id = Action.event_id AND Action.deleted = 0
        INNER JOIN ActionType ON Action.actionType_id = ActionType.id AND ActionType.deleted = 0 AND ActionType.code IN ('amb', 'home')
    WHERE
        Event.deleted = 0
        AND Event.execPerson_id = %(person_id)s
        AND YEAR(Event.execDate) = %(year)s
        AND MONTH(Event.execDate) = %(month)s
        # ActionType.code IN ('amb', 'home')
    ;
    """

    DELETE_DAY_SCHEDULE = """
    DELETE Action
    FROM
        Event
        INNER JOIN Action ON Event.id = Action.event_id AND Action.deleted = 0
        INNER JOIN ActionType ON Action.actionType_id = ActionType.id AND ActionType.deleted = 0 AND ActionType.code IN ('amb', 'home')
    WHERE
        Event.deleted = 0
        AND Event.execPerson_id = %(person_id)s
        AND YEAR(Event.execDate) = %(year)s
        AND MONTH(Event.execDate) = %(month)s
        AND DAY(Event.execDate) = %(day)s
    ;
    """

    SELECT_SCHEDULE_TIME_RANGE = """
    SELECT DISTINCT
        APA_begTime.value AS begTime,
        APA_endTime.value AS endTime,
        ActionType.code AS receptionTypeCode,
        APA_plan.value AS planned,
        APA_extSystem.value AS availableForExternalSystem
    FROM
        Event
        INNER JOIN Action ON Event.id = Action.event_id AND Action.deleted = 0
        INNER JOIN ActionType ON Action.actionType_id = ActionType.id AND ActionType.deleted = 0 AND ActionType.code IN ('amb', 'home')
    
        INNER JOIN ActionPropertyType APT_begTime ON ActionType.id = APT_begTime.actionType_id AND APT_begTime.name = 'begTime{index}'
        INNER JOIN ActionProperty AP_begTime ON Action.id = AP_begTime.action_id AND APT_begTime.id = AP_begTime.type_id
        INNER JOIN ActionProperty_Time APA_begTime ON AP_begTime.id = APA_begTime.id
    
        INNER JOIN ActionPropertyType APT_endTime ON ActionType.id = APT_endTime.actionType_id AND APT_endTime.name = 'endTime{index}'
        INNER JOIN ActionProperty AP_endTime ON Action.id = AP_endTime.action_id AND APT_endTime.id = AP_endTime.type_id
        INNER JOIN ActionProperty_Time APA_endTime ON AP_endTime.id = APA_endTime.id
    
        INNER JOIN ActionPropertyType APT_plan ON ActionType.id = APT_plan.actionType_id AND APT_plan.name = 'plan{index}'
        INNER JOIN ActionProperty AP_plan ON Action.id = AP_plan.action_id AND APT_plan.id = AP_plan.type_id
        INNER JOIN ActionProperty_Integer APA_plan ON AP_plan.id = APA_plan.id
    
        LEFT JOIN ActionPropertyType APT_extSystem ON ActionType.id = APT_extSystem.actionType_id AND APT_extSystem.name = 'notExternalSystems{index}'
        LEFT JOIN ActionProperty AP_extSystem ON Action.id = AP_extSystem.action_id AND APT_extSystem.id = AP_extSystem.type_id
        LEFT JOIN ActionProperty_Integer APA_extSystem ON AP_extSystem.id = APA_extSystem.id
    WHERE
        Event.deleted = 0
        AND Action.id = {action_id}
    """

    SELECT_OFFICE = """
    SELECT 
        OrgStructure.id, 
        OrgStructure.name, 
        OrgStructure.code 
    FROM 
        Person
        INNER JOIN OrgStructure ON Person.orgStructure_id = OrgStructure.id
    WHERE 
        Person.id = %(id)s;
    """

    SELECT_RECEPTION_TYPE = """
    SELECT 
        id, name, code 
    FROM 
        rbReceptionType 
    WHERE 
        code = %(code)s;
    """

    SELECT_PERSON = """
    SELECT 
        id, 
        CONCAT_WS(' ', lastName, firstName, patrName) AS name
    FROM 
        Person
    WHERE
        id = %(id)s;
    """

    SELECT_SPECIALITY = """
    SELECT
        rbSpeciality.id,
        rbSpeciality.name
    FROM
        Person
        INNER JOIN rbSpeciality ON Person.speciality_id = rbSpeciality.id
    WHERE
        Person.id = %(id)s;
    """

    @staticmethod
    def datetime_range(start, end, delta):
        current = start
        if not isinstance(delta, datetime.timedelta):
            delta = datetime.timedelta(**delta)
        while current < end:
            yield current
            current += delta

    @classmethod
    def delete_day(cls, person_id, date):
        import datetime
        date = datetime.datetime.strptime(date, '%Y-%m-%d')
        execute_query(cls.DELETE_DAY_SCHEDULE, params={'person_id': person_id,
                                                       'year': date.year,
                                                       'month': date.month,
                                                       'day': date.day})

    @classmethod
    def create(cls, person_id, date, reception_type, schedules):
        action_type = execute_query(
            cls.SELECT_ACTION_TYPE.format(
                code=reception_type,
            ),
            first=True
        )
        if action_type is None:
            return False

        event_id = execute_query(
            cls.CREATE_EVENT_QUERY.format(
                person_id=person_id,
                date=date,
            ),
        )
        action_id = execute_query(
            cls.CREATE_ACTION_QUERY.format(
                person_id=person_id,
                event_id=event_id,
                action_type_id=action_type['id'],
            ),
        )
        t_number = 0
        property_map = {
            'begTime': '',
            'endTime': '',
            'plan': 0,
            'times': [],
            'queue': [],
        }

        if reception_type == 'amb':
            property_map['office'] = 'cab'
            property_map['colors'] = ''
            property_map['notExternalSystems'] = 0

        for index, schedule in enumerate(schedules):
            for property, value in property_map.items():
                property_name = ('%s%s' % (property, index)) if index else property
                if property == 'times':
                    begin_time = datetime.datetime.strptime(schedule['begTime'], '%H:%M:%S')
                    end_time = datetime.datetime.strptime(schedule['endTime'], '%H:%M:%S')
                    plan = schedule['plan']
                    timedelta = (end_time - begin_time).seconds / 60.0 / float(plan)

                    values = [
                        x.time().strftime('%H:%M:%S')
                        for x in cls.datetime_range(begin_time, end_time, {'minutes': timedelta})
                    ]
                    t_number = len(values)
                elif property == 'queue':
                    values = [None] * t_number
                else:
                    values = schedule.get(property, value)

                if not ActionProperty.create_property(property_name, action_id, person_id, values):
                    execute_query("DELETE FROM Event WHERE id = %s" % event_id)
                    assert False, 'Schedule: property %s not created' % property_name

    @classmethod
    def get(cls, person_id, start_date):
        data = {
            'schedule': [],
            'quotas': [],
            'start_date': start_date.strftime("%Y-%m"),
            'person': execute_query(
                cls.SELECT_PERSON,
                params={'id': person_id},
                first=True,
            )}
        data['person']['office'] = execute_query(
            cls.SELECT_OFFICE,
            params={'id': person_id},
            first=True
        )
        data['person']['speciality'] = execute_query(
            cls.SELECT_SPECIALITY,
            params={'id': person_id},
            first=True
        )

        schedule_map = OrderedDict()
        for date in filter(lambda x: x != 0, sum(calendar.monthcalendar(start_date.year, start_date.month), [])):
            formatted_date = str(datetime.date(start_date.year, start_date.month, date))
            schedule_map[formatted_date] = {
                'busy': False,
                'date': str(datetime.date(start_date.year, start_date.month, date)),
                'scheds': [],
                'roa': None,
            }

        for schedule in execute_query(
                cls.SELECT_SCHEDULE,
                params={'person_id': person_id, 'year': start_date.year, 'month': start_date.month}
        ):
            stmt = []
            for index in range(0, 11):
                stmt.append(cls.SELECT_SCHEDULE_TIME_RANGE.format(
                    action_id=schedule['action_id'],
                    index=index if index else ''
                ))

            for scheds in execute_query('\nUNION\n'.join(stmt), params=None):
                schedule_map[str(schedule['date'])]['scheds'].append(
                    {
                        'begTime': (datetime.datetime.min + scheds['begTime']).time().strftime("%H:%M:%S"),
                        'endTime': (datetime.datetime.min + scheds['endTime']).time().strftime("%H:%M:%S"),
                        'office': data['person']['office'],
                        'reception_type': execute_query(
                            cls.SELECT_RECEPTION_TYPE,
                            params={'code': scheds['receptionTypeCode']},
                            first=True
                        ),
                        'planned': scheds['planned'],
                        'availableForExternalSystem': scheds['availableForExternalSystem'],
                    }
                )

        data['schedule'] = list(schedule_map.values())
        return data


def convert_schedule(data):
    return {
        'begTime': data['begTime'],
        'endTime': data['endTime'],
        'plan': data['planned'],
        'office': 'cab',
        'times': [],
        'queue': None,
        'reception_type': data['reception_type']['code'],
        'notExternalSystems': data['availableForExternalSystem'],
    }


class PersonSchedule(Resource):
    schema = schemas.PersonSchema()

    def get(self):
        try:
            marsh_result = self.schema.load(request.args)
            if marsh_result.errors:
                return make_response(jsonify(error='Bad Request'), 400)
            return make_response(jsonify(Schedule.get(**marsh_result.data)), 200)
        except Exception as e:
            return make_response(jsonify(error=e), 400)

    def post(self):
        try:
            data = request.get_json(force=True)
            for schedule in data['schedule']:
                schedules_map = {
                    'amb': [],
                    'home': [],
                }
                for time_line in schedule['scheds']:
                    time_line = convert_schedule(time_line)

                    if time_line['reception_type'] not in schedules_map:
                        assert False, 'incorrect `reception_type` value'

                    schedules_map[time_line['reception_type']].append(time_line)

                # assert schedules_map['amb'] or schedules_map['home'], 'incorrect `scheds` values'

                Schedule.delete_day(data['person_id'], schedule['date'])
                for type, values in schedules_map.items():
                    Schedule.create(data['person_id'], schedule['date'], type, values)
            return make_response(
                jsonify(
                    Schedule.get(
                        person_id=data['person_id'],
                        start_date=datetime.datetime.strptime(data['start_date'], "%Y-%m")
                    )
                ),
                200
            )
        except Exception as e:
            return make_response(jsonify(error=e), 400)
