from api import rb
from api.schedule.person_list import schemas


class PersonList(rb.base.BaseRefBook):
    TABLE_NAME = 'vrbPersonWithSpecialityAndOrganisation'

    COLUMNS = ('*',)

    schema = schemas.PersonListSchema()
