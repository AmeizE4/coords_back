from marshmallow import Schema, fields


class PersonListSchema(Schema):
    id = fields.Int(
        required=False,
        attribute='id'
    )

    code = fields.Raw(
        required=False,
        attribute='code'
    )

    lastName = fields.Raw(
        required=False,
        attribute='lastName'
    )

    firstName = fields.Raw(
        required=False,
        attribute='firstName'
    )

    patrName = fields.Raw(
        required=False,
        attribute='patrName'
    )

    specialityName = fields.Raw(
        required=False,
        attribute='specialityName'
    )

    specialityCode = fields.Raw(
        required=False,
        attribute='specialityCode'
    )

    orgFullName = fields.Raw(
        required=False,
        attribute='orgFullName'
    )

    orgShortName = fields.Raw(
        required=False,
        attribute='orgShortName'
    )

    orgStructureCode = fields.Raw(
        required=False,
        attribute='orgStructureCode'
    )

    orgStructureName = fields.Raw(
        required=False,
        attribute='orgStructureName'
    )

    speciality_id = fields.Int(
        required=False,
        attribute='speciality_id'
    )

    org_id = fields.Int(
        required=False,
        attribute='org_id'
    )

    orgStructure_id = fields.Int(
        required=False,
        attribute='orgStructure_id'
    )
