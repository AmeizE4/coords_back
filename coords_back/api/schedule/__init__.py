from api.schedule.person import PersonSchedule
from api.schedule.person_list import PersonList

RESOURCE_LIST = [
    (PersonList, '/schedule/person_list'),
    (PersonSchedule, '/schedule/person'),
]
