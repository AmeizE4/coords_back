from marshmallow import Schema, fields, validates, ValidationError

from database import execute_query


class HistorySchema(Schema):
    patId = fields.Int(
        required=True,
        attribute='pat_id'
    )

    @validates('patId')
    def validate_pat_id(self, pat_id):
        client = execute_query(
            "SELECT id FROM Client WHERE id=%(pat_id)s",
            {"pat_id": pat_id}
        )
        if not client:
            raise ValidationError('no such client')


class HistoryVisitsSchema(Schema):
    clientId = fields.Int(required=True, attribute='client_id')

    @validates('clientId')
    def validate_client(self, client_id):
        client = execute_query(
            "SELECT id FROM Client WHERE id=%(id)s",
            {"id": client_id},
            first=True
        )
        if not client:
            raise ValidationError('no such client')


class ClientJobTicketSchema(Schema):
    clientId = fields.Int(
        required=True,
        attribute='client_id',

    )
    searchFrom = fields.Date(
        required=True,
        attribute='search_from',
        format='%Y-%m-%d'
    )
    searchTo = fields.Date(
        required=True,
        attribute='search_to',
        format='%Y-%m-%d'
    )

    @validates('clientId')
    def validate_pat_id(self, client_id):
        client = execute_query(
            "SELECT id FROM Client WHERE id=%(client_id)s",
            {"client_id": client_id}
        )
        if not client:
            raise ValidationError('no such client')
