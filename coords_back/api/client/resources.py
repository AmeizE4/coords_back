from flask import request, make_response, jsonify
from flask_restful import Resource

import const
from database import execute_query
from .schemas import HistorySchema, HistoryVisitsSchema, ClientJobTicketSchema


class HistoryResource(Resource):
    schema = HistorySchema()

    def get(self):
        # language=yaml
        """
        История пациента
        ---
        tags:
          - client
        description: Возвращает историю пациента по id
        parameters:
          - in: query
            name: patId
            type: number
            required: true
            description: Идентификатор пациента
        responses:
          '200':
            description: >
              Запрос успешен


              ambActionId - идентификатор очереди врача (в паре с index'ом можно
              удалять и перемещать данную запис методами /appointment DELETE и
              PUT)
            schema:
              type: object
              properties:
                visits:
                  type: array
                  items:
                    type: object
                    properties:
                      startDate:
                        type: string
                        example: YYYY-mm-dd
                      startTime:
                        type: string
                        example: HH:MM:SS
                      doctor:
                        type: object
                        properties:
                          name:
                            type: string
                            example: Иванов И. И.
                          pk:
                            type: object
                            properties:
                              doctorId:
                                type: number
                                format: int32
                      spec:
                        type: object
                        properties:
                          name:
                            type: string
                            example: Врач скорой помощи
                          pk:
                            type: object
                            properties:
                              specId:
                                type: number
                                format: int32
                      index:
                        type: number
                      ambActionId:
                        type: number
          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)
        data = marsh_result.data

        history = execute_query(
            const.select_history,
            data
        )
        answer = []
        for row in history:
            answer.append({
                "startDate": row['date'],
                "startTime": row['time'],
                "doctor": {
                    "name": "{0} {1} {2}".format(
                        row['docLastName'],
                        row['docFirstName'],
                        row['docPatrName']
                    ),
                    "pk": {
                        "doctorId": row['docId']
                    },
                },
                "spec": {
                    "name": row['specName'],
                    "pk": {
                        "specId": row['specId']
                    },
                },
                "index": row['index'],
                "ambActionId": row['ambActionPropId']
            })
        return {"visits": answer}


class HistoryVisitsResource(Resource):
    schema = HistoryVisitsSchema()

    def get(self):
        # language=yaml
        """
        История посещений
        ---
        tags:
        - client
        description: >
          Возвращает историю посещений клиента.
          Смотрим в таблицу Visit.
        parameters:
        - in: query
          name: clientId
          description: Идентификатор клиента
          type: number
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: array
              items:
                type: object
                properties:
                  date:
                    type: string
                  fio:
                    type: string
                  specName:
                    type: string
          '400':
            description: Bad request
        """

        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)
        data = marsh_result.data

        answer = execute_query(
            """
            SELECT DATE_FORMAT(E.execDate, '%%Y-%%m-%%d')                AS date,
                   CONCAT(P.lastName, ' ', P.firstName, ' ', P.patrName) AS `fio`,
                   rS.name                                               AS specName
            FROM Event E
                   INNER JOIN EventType ET_graphic ON E.eventType_id = ET_graphic.id AND ET_graphic.code = '01'
                   LEFT JOIN Person P ON E.execPerson_id = P.id
                   LEFT JOIN rbSpeciality rS ON P.speciality_id = rS.id
            WHERE E.client_id = %(client_id)s
            ORDER BY E.execDate DESC
            """,
            data
        )
        return answer


class ClientJobTicketResource(Resource):
    schema = ClientJobTicketSchema()

    def get(self):
        # language=yaml
        """
        Анализы клиента
        ---
        tags:
        - client
        description: Получить список анализов(назначений) клиента
        parameters:
        - in: query
          description: Идентификатор клиента
          name: clientId
          type: number
        - in: query
          description: >
            Дата, с которой искать списки назначений, формат YYYY-mm-dd
          name: searchFrom
          type: string
        - in: query
          description: >
            Дата, по которую искать списки назначений, формат YYYY-mm-dd
          name: searchTo
          type: string
        responses:
          '200':
            description: >
              Запрос успешен.

              Статусы jobTicket: 0 - ожидание, 1 - выполнение, 2 - закончено.

              Статусы action: 0 - Начато, 1 - ожидание, 2 - закончено,
              3 - отменено, 4 - без результата.
            schema:
              type: array
              items:
                type: object
                properties:
                  jobTicket:
                    type: object
                    properties:
                      typeName:
                        type: string
                      status:
                        type: number
                      date:
                        type: string
                  actions:
                    type: array
                    items:
                      type: object
                      properties:
                        status:
                          type: number
                        typeName:
                          type: string
                        endDate:
                          type: string

          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        data = marsh_result.data

        job_tickets = execute_query(
            """
            SELECT J_T.id as jobTicket_id,
                   J_Type.name as jobTicketType,
                   DATE_FORMAT(J_T.datetime, '%%Y-%%m-%%d') as date,
                   J_T.status as jobTicketStatus,
                   A.status as actionStatus,
                   AType.name as actionTypeName,
                   DATE_FORMAT(A.endDate, '%%Y-%%m-%%d') AS endDate
            FROM Event E
                   LEFT JOIN Action A ON E.id = A.event_id
                   LEFT JOIN ActionType AType ON A.actionType_id = AType.id
                   LEFT JOIN ActionProperty AP on A.id = AP.action_id
                   LEFT JOIN ActionPropertyType APT on AP.type_id = APT.id
                   INNER JOIN ActionProperty_Job_Ticket AP_J_T on AP.id = AP_J_T.id
            
                   LEFT JOIN Job_Ticket J_T on AP_J_T.value = J_T.id
                   LEFT JOIN Job J on J_T.master_id = J.id
                   LEFT JOIN rbJobType J_Type on J.jobType_id = J_Type.id
            WHERE 1 = 1
              AND E.client_id = %(client_id)s
              AND J_T.datetime between %(search_from)s and %(search_to)s
            ORDER BY A.endDate
            """,
            data
        )
        answer = {
            j['jobTicket_id']: {
                "jobTicket": {
                    "typeName": j['jobTicketType'],
                    "date": j['date'],
                    "status": j['jobTicketStatus'],
                },
                'actions': []
            } for j in job_tickets
        }
        for elem in job_tickets:
            answer[elem['jobTicket_id']]['actions'].append({
                "status": elem['actionStatus'],
                "typeName": elem['actionTypeName'],
                "endDate": elem['endDate']
            })
        return list(answer.values())
