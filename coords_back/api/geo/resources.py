from flask import make_response, jsonify, request
from flask_restful import Resource

import const
from database import execute_query
from .schemas import GeoDoctorsSchema, PointSchema
from utils import list_to_sql_array, list_to_sql_array_with_wrap


class UserResource(Resource):

    def get(self, person_id):
        # language=yaml
        """
        Пользователель по логину
        ---
        tags:
        - geo
        description: >
          Возвращает последнюю точку одного пользователя.

          Отключено т.к. логины могут содержать пробелы, API к такому не готова
          :)
        parameters:
        - in: path
          name: person_id
          type: number
          description: Логин пользователя
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: object
              properties:
                person:
                  type: object
                  properties:
                    fio:
                      type: string
                    id:
                      type: number
                      format: int32
                    last_point:
                      type: object
                      properties:
                        lat:
                          type: number
                          format: float
                        long:
                          type: number
                          format: float
                        createDate:
                          type: string
                          example: "2018-11-22"
                        createTime:
                          type: string
                          example: "16:45:41"
        """
        person = execute_query(
            """
            SELECT p.id, CONCAT(p.lastName, ' ', p.firstName, ' ', p.patrName) AS `fio`
            FROM Person p
            WHERE p.id = %(person_id)s
            """,
            {"person_id": person_id},
            first=True
        )
        if not person:
            return make_response(jsonify(error="Bad Request"), 400)

        person['last_point'] = execute_query(
            """
            SELECT lat, `long`, MAX(createDate), MAX(createTime)
            FROM Geolocation
              LEFT JOIN Person P on Geolocation.person_id = P.id
            WHERE person_id = %(person_id)s
            """,
            {"person_id": person.get('id')},
            first=True
        )
        return {"person": person}


class GeoDoctorsResource(Resource):
    schema = GeoDoctorsSchema()

    def get(self):
        # language=yaml
        """
        Врачи на карте
        ---
        tags:
        - geo
        description: >
          Возвращает список врачей с последней сохраненной координатой,
          добавленной в текущий день.

          Если даты не указаны, поиск идет на две недели от сегодняшнего дня.
          Если не указана одна из дат, вернет ошибку.
        parameters:
        - in: query
          name: searchFrom
          type: string
          description: Дата, с которой идет поиск врачей
        - in: query
          name: searchTo
          type: string
          description: Дата, по которую идет поиск врачей
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: array
              items:
                type: object
                properties:
                  fio:
                    type: string
                  id:
                    type: number
                    format: int32
                  last_point:
                    type: object
                    properties:
                      lat:
                        type: number
                        format: float
                      long:
                        type: number
                        format: float
                      createDate:
                        type: string
                        example: "2018-11-22 16:45:41"
          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        data = marsh_result.data

        persons = execute_query(
            const.select_doctors_home,
            data
        ) or []
        answer = []
        for person in persons:
            answer.append({
                "id": person['id'],
                "fio": person['fio'],
                "last_point": {
                    "lat": person['lat'],
                    "long": person['long'],
                    "createDate": person['createDate'],
                } if person['lat'] is not None and person['long'] is not None else None
            })
        return make_response(jsonify(answer))


class PointResource(Resource):
    schema = PointSchema()

    def post(self):
        # language=yaml
        """
        Добавить точку
        ---
        tags:
        - geo
        description: Позволяет добавить точку для конкретного врача
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              person_id:
                type: number
              login:
                type: string
              lat:
                type: number
                format: float
              long:
                type: number
                format: float
          description: >
            В первую очередь идет поиск по id, в следующую - по логину.

            В будующих версиях поиск по логину будет отключен.
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)

        point_record = marsh_result.data
        execute_query(
            "INSERT INTO Geolocation({}) VALUES({})".format(
                list_to_sql_array(point_record),
                list_to_sql_array_with_wrap(point_record)
            ),
            point_record
        )
        return make_response(jsonify(info="OK"), 200)
