from datetime import datetime

from marshmallow import Schema, fields, validates, ValidationError, post_load

from base_schemas import SearchFromToBaseSchema
from database import execute_query


class GeoDoctorsSchema(SearchFromToBaseSchema):
    pass


class PointSchema(Schema):
    person_id = fields.Int()
    login = fields.Str()
    lat = fields.Float(required=True)
    long = fields.Float(
        required=True,
        # reserved world in mysql
        attribute="`long`"
    )

    @validates('person_id')
    def validate_doctor(self, person_id):
        doctor = execute_query("select id from Person WHERE id=%(id)s",
                               {'id': person_id},
                               first=True) or {}
        if not doctor:
            raise ValidationError('no such person')

    @validates('login')
    def validate_doctor(self, login):
        doctor = execute_query("select id from Person WHERE login=%(login)s",
                               {'login': login},
                               first=True) or {}
        if not doctor:
            raise ValidationError('no such person')

    @post_load()
    def make_point_record(self, data):
        # for older version save opportunity to save point via login
        if not data.get('person_id'):
            if not data.get('login'):
                raise ValidationError('Must contain person_id or login')
            data['person_id'] = execute_query(
                "select id from Person WHERE login=%(login)s",
                {'login': data['login']},
                first=True
            )['id']
        data['createDate'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        data.pop('login', None)
        return data
