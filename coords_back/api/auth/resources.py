from bcrypt import hashpw, checkpw, gensalt
from datetime import datetime, timedelta
from secrets import token_hex

import requests
from flask import request, make_response, jsonify
from flask_restful import Resource, abort

from config import config
from database import execute_query, get_db
from .schemas import RegisterSchema, LoginSchema, SelectPerson, RegisterWithBaseOrgStructureSchema
from utils import list_to_sql_array, list_to_sql_array_with_wrap


def abort_if_error(code):
    if code // 100 > 2:
        abort(code)


def make_token(user_id):
    """
    Creating session record for user by user_id, return auth token
    """
    token = token_hex(64)
    session_record = {
        "user_id": user_id,
        "token": token,
        "createDateTime": datetime.now(),
        "expireDateTime": datetime.now() + timedelta(minutes=config['org_vars']['ttl'])
    }
    execute_query(
        "INSERT INTO backend_logger.Session({0}) VALUES({1})".format(
            list_to_sql_array(session_record),
            list_to_sql_array_with_wrap(session_record)
        ),
        session_record
    )
    return token


def hub_registration(data):
    user = {
        "email": data['person']['email'],
        "info": {
            "firstName": data['person']['firstName'],
            "lastName": data['person']['lastName'],
            "patrName": data['person']['patrName'],
            "type": 'DOCTOR',
            "speciality": data['person']['specialityId']
        },
        "password": data['person']['password'],
        "username": data['person']['username'],
    }
    ur_answer = requests.post('http://85.143.210.27:8010/user/reg', json=user)
    abort_if_error(ur_answer.status_code)


def hub_person_attach(data, host_id=3):
    access_token_answer = requests.post(
        'http://85.143.210.27:8010/oauth/token',
        {"username": data['person']['username'], "password": data['person']['password'], 'grant_type': 'password'},
        headers={"Authorization": "Basic ZG9jdG9yYXBwOjIxQUQwRDJCRkY2RDRGQ0E5RTEyM0U5NTE2N0Y0RTE0"}
    )
    abort_if_error(access_token_answer.status_code)
    access_token_answer.raise_for_status()
    access_token = access_token_answer.json()['access_token']

    person_attach = {
        "login": data['person']['username'],
        "password": data['person']['password_hashed'],
        "hostId": host_id,
    }
    pa_answer = requests.post('http://85.143.210.27:8010/person/attach', json=person_attach,
                              headers={"Authorization": "Bearer {}".format(access_token)})
    abort_if_error(pa_answer.status_code)


class RegisterResource(Resource):
    schema = RegisterSchema()

    def post(self):
        # language=yaml
        """
        Регистрация юзера
        ---
        tags:
        - auth
        description: Позволяет сделать регистрацию юзера
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              login:
                type: string
              password:
                type: string
              email:
                type: string
        responses:
          '200':
              description: Если авторизация успешна - возвращает токен
              schema:
                type: object
                properties:
                  token:
                    type: string
          '400':
              description: Bad Request
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data
        login = data['login']
        password = hashpw(data['password'].encode(), gensalt())
        email = data.get('email')

        now = datetime.now()
        user_record = {
            "login": login,
            "password": password,
            "email": email,
            "createDateTime": now,
        }
        user_id = execute_query(
            "INSERT INTO User({0}) VALUES({1})".format(
                list_to_sql_array(user_record),
                list_to_sql_array_with_wrap(user_record)
            ),
            user_record
        )
        token = make_token(user_id)
        return make_response(jsonify(token=token), 201)


class LogInResource(Resource):
    schema = LoginSchema()

    def post(self):
        # language=yaml
        """
        Log In
        ---
        tags:
        - auth
        description: >
          Позволяет юзеру пройти авторизацию.
          С токеном и person'ами надо идти в метод /selectPerson.
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              login:
                type: string
              password:
                type: string
        responses:
          '200':
              schema:
                type: object
                properties:
                  token:
                    type: string
                  persons:
                    type: object
                    id:
                      type: number
          '400':
              description: Bad Request
          '401':
              description: Unauthorized
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)
        data = marsh_result.data

        login = data['login']
        password = data['password']

        user = execute_query(
            "SELECT * FROM User WHERE login=%(login)s",
            {"login": login},
            first=True
        )
        if user and not checkpw(password.encode(), user['password']):
            return make_response(jsonify(error='Unauthorized'), 401)

        token = make_token(user['id'])

        answer = {
            "token": token,
            'persons': execute_query(
                """
                SELECT P.id, P.firstName, P.lastName, rS.id, rS.name, OS.id, OS.name
                FROM Person P
                    LEFT JOIN rbSpeciality rS on P.speciality_id = rS.id
                    LEFT JOIN OrgStructure OS on P.orgStructure_id = OS.id
                WHERE P.user_id = %(user_id)s
                """,
                {"user_id": user['id']}
            )}
        return make_response(jsonify(answer), 200)


class SelectPersonResource(Resource):
    schema = SelectPerson()

    def post(self):
        # language=yaml
        """
        Выбор профиля
        ---
        tags:
        - auth
        description: >
          Последний этап авторизации, выбор профиля, под которым будет
          проводиться дальнейшая сессия.
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              token:
                type: string
              personId:
                type: number
        responses:
          '200':
              description: Запрос успешен
          '400':
              description: Bad Request
          '401':
              description: Unauthorized
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data
        execute_query(
            """
            UPDATE backend_logger.Session s
            SET s.person_id = %(person_id)s
            WHERE s.id = %(session_id)s
            """,
            {
                "person_id": data['person_id'],
                "session_id": data['session_id'],
            }
        )
        return make_response(jsonify(info="OK"), 200)


class RegisterWithBaseOrgStructureResource(Resource):
    schema = RegisterWithBaseOrgStructureSchema()

    def post(self):
        # language=yaml
        """
        Создание Person с OrgStructure
        ---
        tags:
        - auth
        description: >
          Создает Person'а с orgStructure'ой по фио врача. Оргструктура привязывается
          к единой организации, id которой необходимо указать в конфиге (иначе метод будет падать).
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              username:
                type: string
              password:
                type: string
              lastName:
                type: string
              firstName:
                type: string
              patrName:
                type: string
              specialityId:
                type: number
              address:
                type: string
              email:
                type: string
              educations:
                type: array
                items:
                  type: object
                  properties:
                    specialityId:
                      type: number
                    date:
                      type: string
                    serial:
                      type: string
                    number:
                      type: string
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        data = marsh_result.data
        get_db().begin()
        try:
            now = datetime.now()
            person_record = {
                "lastName": data['person']['lastName'],
                "firstName": data['person']['firstName'],
                "patrName": data['person']['patrName'],
                "speciality_id": data['person']['specialityId'],
                "login": data['person']['username'],
                "password": data['person']['password_hashed'],
                "createDatetime": now,
                "modifyDatetime": now,
                "userProfile_id": 29,
                "code": "",
                "federalCode": "",
                "regionalCode": "",
                "office": "",
                "office2": "",
                "ambPlan": 0,
                "ambPlan2": 0,
                "ambNorm": 0,
                "homPlan": 0,
                "homPlan2": 0,
                "homNorm": 0,
                "expPlan": 0,
                "expNorm": 0,
                "retired": 0,
                # TODO Alter table and make it nullable
                "birthDate": "",
                "birthPlace": "",
                "sex": 0,
                "SNILS": "",
                "INN": "",
                "academicDegree": 0,
                "typeTimeLinePerson": 0,
            }
            person_id = execute_query(
                "INSERT INTO Person({}) VALUES({})".format(
                    list_to_sql_array(person_record),
                    list_to_sql_array_with_wrap(person_record)
                ),
                person_record
            )
            fio = "{} {} {}".format(data['person']['lastName'], data['person']['firstName'], data['person']['patrName'])
            org_structure_record = {
                "chief_id": person_id,
                "createDateTime": now,
                "modifyDatetime": now,
                "code": '',
                "name": fio,
                "address": data['address'],
                "organisation_id": config['org_vars']['rootOrganisationId'],
                "infisCode": "",
                "infisInternalCode": "",
                "infisDepTypeCode": "",
                "infisTariffCode": "",
                "bookkeeperCode": "",
                "storageCode": "",
            }
            org_structure_id = execute_query(
                "INSERT INTO OrgStructure({}) VALUES({})".format(
                    list_to_sql_array(org_structure_record),
                    list_to_sql_array_with_wrap(org_structure_record),
                ),
                org_structure_record
            )
            execute_query(
                """
                UPDATE Person
                SET orgStructure_id = %(org_id)s
                WHERE id = %(person_id)s
                """,
                {
                    "org_id": org_structure_id,
                    "person_id": person_id
                }
            )
            for e in data['educations']:
                education_record = {
                    "createDatetime": now,
                    "modifyDatetime": now,
                    "master_id": person_id,
                    "serial": e['serial'],
                    "number": e['number'],
                    "date": e['date'],
                    "origin": '',
                    "speciality_id": e['speciality_id'],
                    "deleted": 0,
                    "status": '',
                }
                execute_query(
                    "INSERT INTO Person_Education({0}) VALUES({1})".format(
                        list_to_sql_array(education_record),
                        list_to_sql_array_with_wrap(education_record)
                    ),
                    education_record
                )
            hub_registration(data)
            get_db().commit()
            hub_person_attach(data)
        except Exception as e:
            get_db().rollback()
            raise e
        return make_response(jsonify(info='OK'), 200)
