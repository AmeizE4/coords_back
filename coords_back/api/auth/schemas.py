from hashlib import md5

from marshmallow import Schema, fields, ValidationError, post_load

from database import execute_query


class RegisterSchema(Schema):
    login = fields.Str(required=True)
    password = fields.Str(required=True)
    email = fields.Email()


class LoginSchema(Schema):
    login = fields.Str(required=True)
    password = fields.Str(required=True)


class SelectPerson(Schema):
    token = fields.Str(required=True)
    personId = fields.Str(required=True, attribute="person_id")

    @post_load
    def compare_user_and_person(self, data):
        session = execute_query(
            """
            SELECT id, user_id
            FROM backend_logger.Session
            WHERE token=%(token)s
            """,
            {"token": data['token']},
            first=True
        )
        person = execute_query(
            "SELECT user_id FROM Person WHERE id =  %(person_id)s",
            {"person_id": data['person_id']},
            first=True
        )
        if not (session and person and session['user_id'] == person['user_id']):
            raise ValidationError('')
        data['session_id'] = session['id']

        return data


class RegisterWithBaseOrgStructureSchema(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    lastName = fields.Str(required=True)
    firstName = fields.Str(required=True)
    patrName = fields.Str()
    specialityId = fields.Int(required=True)
    email = fields.Email(required=True)

    educations = fields.Nested('EducationSchema', many=True, missing=[])

    address = fields.Str(required=True)

    @post_load
    def make_post_load(self, data):
        data['password_hashed'] = md5(data['password'].encode()).hexdigest()
        return {
            "educations": data.pop("educations", None),
            "address": data.pop("address", None),
            'person': data
        }


class EducationSchema(Schema):
    specialityId = fields.Int(
        required=True,
        attribute='speciality_id'
    )
    date = fields.Date(
        required=True,
        format='%Y-%m-%d'
    )
    serial = fields.Str(
        required=True
    )
    number = fields.Str(
        required=True
    )
