from datetime import datetime

from flask import request, make_response, jsonify
from flask_restful import Resource

import const
from config import config
from database import execute_query, get_db
from .schemas import DoctorSchema, TimeTableSchema, AppointmentSchema, AppointmentsSchema, \
    WaitingListSchema, QueueMoveSchema
from utils import list_to_sql_array_with_wrap, list_to_sql_array


def get_org_structs():
    result = set()
    root_org_struct_id = config['org_vars']['rootOrgStructId']

    # TODO Заглушка для более старых версий. Убрать как обновятся все конфиги
    if not isinstance(root_org_struct_id, list):
        root_org_struct_id = [root_org_struct_id]
    # заполняем подразделения организации, если еще не заполнено
    if not result:
        result = set(root_org_struct_id)

        def create_org_list(struct_id, res):
            orgs = execute_query(
                """
                select id
                from OrgStructure o
                where o.deleted = 0
                    and o.parent_id = %(struct_id)s
                """,
                {'struct_id': struct_id}
            )
            for o in orgs:
                res.add(o['id'])
                create_org_list(o['id'], res)

        for org in root_org_struct_id:
            create_org_list(org, result)

    return result


orgStructs = get_org_structs()


class DoctorsResource(Resource):
    schema = DoctorSchema()

    def get(self):
        # language=yaml
        """
        Список врачей по специальностям
        ---
        tags:
        - timeTable
        description: >
            Возвращает словарь специальностей и врачей, имеющих
            данную специальность
        parameters:
        - in: query
          name: searchFrom
          type: string
        - in: query
          name: searchTo
          type: string
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: object
              properties:
                specName:
                  type: array
                  items:
                    type: object
                    properties:
                      doctorId:
                        type: object
                        properties:
                          doctorId:
                            type: number
                            format: int32
                          clinicId:
                            type: number
                            format: int32
                      name:
                        type: string
                        example: Иванов Иван Иванович
                      ticketCount:
                        type: number
                        format: int32
                      nearestDate:
                        type: string
                        example: YYYY-mm-dd
                      lastDate:
                        type: string
                        example: YYYY-mm-dd
                      specialityId:
                        type: number
                        format: int32

        """
        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)
        data = marsh_result.data

        doctors = execute_query(
            const.select_doctors,
            dict(data, **{'orgStructs': orgStructs})
        )
        answer = {}
        for doctor in doctors:
            spec_name = doctor['specName']
            if not answer.get(spec_name):
                answer[spec_name] = []
            answer[spec_name].append({
                "doctorId": {
                    "doctorId": doctor['personId'],
                    "clinicId": doctor['clinicId']
                },
                "name": doctor['fio'],
                'ticketCount': doctor['available'],
                "nearestDate": doctor['minDate'],
                "lastDate": doctor['maxDate'],
                "specialityId": doctor['specId']
            })
        return make_response(jsonify(answer))


class TimeTableResource(Resource):
    schema = TimeTableSchema()

    def get(self):

        # language=yaml
        """
        Расписание врача
        ---
        tags:
        - timeTable
        description: >
          Позволяет получить расписание конкретного врача в указанный период

          Возвращает словарь, где каждый ключ - это дата приема, и по ключу
          лежат список тикетов по данной дате
        parameters:
        - in: query
          name: idDoctor
          type: number
          required: true
          description: Идентификатор врача
        - in: query
          name: searchFrom
          type: string
          required: true
          description: Начальная дата выборки формата - YYYY-mm-dd
        - in: query
          name: searchTo
          type: string
          required: true
          description: Конечная дата выборки формата - YYYY-mm-dd
        responses:
          '200':
            description: Запрос успешен
            schema:
             type: object
             properties:
              '2018-12-14':
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: object
                      properties:
                        clinicId:
                          type: number
                          format: int32
                        appointmentId:
                          type: number
                          format: int32
                    doctorId:
                      type: number
                      format: int32
                    client:
                      type: object
                      properties:
                        id:
                          type: number
                          format: int32
                        client_last_name:
                          type: string
                          example: Иванович
                        client_first_name:
                          type: string
                          example: Иван
                        client_patr_name:
                          type: string
                          example: Иванов
                    ticketIndex:
                      type: number
                      format: int32
                    ticketTime:
                      type: string
                      example: HH:MM:SS
                    value:
                      type: number
                      format: int32
          '400':
            description: Bad Request
        """
        marsh_result = self.schema.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)
        data = marsh_result.data
        # select working dates
        dates = execute_query(
            const.select_dates,
            data
        )
        answer = {date['date']: [] for date in dates}
        if not answer:
            return make_response(
                jsonify(error='В данном диапазоне у врача нет свободных номерков'),
                400
            )
        # select appoints
        args = {date: date for date in answer}
        args["id_doctor"] = data['id_doctor']
        appoints = execute_query(
            const.select_all_appointments.format(
                list_to_sql_array_with_wrap(answer)
            ),
            args
        )
        # formatting answer
        for appoint in appoints:
            date = appoint.pop('date').strftime('%Y-%m-%d')
            answer[date].append({
                "id": {
                    'clinicId': appoint['clinicId'],
                    'appointmentId': appoint['queueActionId']
                },
                "doctorId": appoint['person_id'],
                "client": {
                    "id": appoint['client_id'],
                    "client_last_name": appoint['client_last_name'],
                    "client_first_name": appoint['client_first_name'],
                    "client_patr_name": appoint['client_patr_name']
                },
                "ticketIndex": appoint['ticketIndex'],
                "ticketTime": appoint['ticketTime'],
                "value": appoint['value']
            })
        return answer


class AppointmentResource(Resource):
    schema = AppointmentSchema()

    def put(self):
        # language=yaml
        """
        Перенос записи
        ---
        tags:
        - timeTable
        description: Переносит запись пациента
        parameters:
        - in: body
          name: body
          description: >
            * oldAppointment - запись, КОТОРУЮ нужно перенести

            * newAppointment - запись, КУДА происходит перенос
          schema:
            type: object
            properties:
              oldAppointment:
                type: object
                properties:
                  queueId:
                    type: number
                    format: int32
                  ticketIndex:
                    type: number
                    format: int32
              newAppointment:
                type: object
                properties:
                  queueId:
                    type: number
                    format: int32
                  ticketIndex:
                    type: number
                    format: int32
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: >
              1 Bad Request - указаны не все данные или данные указаны неверно

              2 На данное время отсутствует запись

              3 Запись сверхочереди нельзя переносить

              4 Нельзя переместить прошедшую запись
        """
        marsh_result_old = self.schema.load(request.get_json().get("oldAppointment", {}))
        marsh_result_new = self.schema.load(request.get_json().get("newAppointment", {}))

        if marsh_result_new.errors or marsh_result_old.errors:
            return make_response(jsonify(error="Bad Request"), 400)

        old_appoint = marsh_result_old.data
        new_appoint = marsh_result_new.data

        old_appoint_checks = execute_query(
            const.select_AP_A_for_appoint_res,
            old_appoint,
            first=True
        ) or {}  # Hack if old_appoint_checks return's emtpy
        value = old_appoint_checks.get('value')
        if not value:
            return make_response(
                jsonify(
                    error="На данное время отсутствует запись"
                ), 400)
        if not old_appoint_checks.get('time'):
            return make_response(
                jsonify(
                    error="Запись сверхочереди нельзя переносить"
                ), 400)

        # if client try to move past app-t - return error
        a_dt = datetime.strptime(
            "{0} {1}".format(
                old_appoint_checks['setDate'],
                old_appoint_checks['time']
            ),
            '%Y-%m-%d %H:%M:%S'
        )
        if a_dt < datetime.now():
            return make_response(jsonify(
                error="Нельзя переместить прошедшую запись"
            ), 400)

        get_db().begin()
        try:
            execute_query(
                """
                UPDATE ActionProperty_Action AP_A
                SET AP_A.value = %(value)s
                WHERE AP_A.id = %(queue_id)s
                    AND AP_A.`index` = %(index)s
                """,
                {"value": value, **new_appoint}
            )
            execute_query(
                """
                UPDATE ActionProperty_Action AP_A
                SET AP_A.value = NULL
                WHERE AP_A.id = %(queue_id)s
                    AND AP_A.`index` = %(index)s
                """,
                {**old_appoint}
            )
            get_db().commit()
        except Exception as e:
            get_db().rollback()
            raise e

        return make_response(jsonify(info="OK"), 200)

    def delete(self):
        # language=yaml
        """
        Отмена записи
        ---
        tags:
        - timeTable
        description: Отмена записи к врачу
        parameters:
        - in: body
          name: body
          schema:
            type: object
            properties:
              queueId:
                type: number
                format: int32
              ticketIndex:
                type: number
                format: int32
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: >
              1 Bad Request - указаны не все данные или данные указаны неверно

              2 На данное время отсутствует запись (т.е. нечего удалять)

              3 Нельзя удалять прошедшую запись (т.е. нечего удалять)
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        data = marsh_result.data

        to_delete = execute_query(
            const.select_AP_A_for_appoint_res,
            data,
            first=True
        ) or {}  # Hack if to_delete return's emtpy

        if not to_delete.get('value'):
            return make_response(
                jsonify(
                    error="На данное время отсутствует запись"
                ), 400)

        get_db().begin()
        try:
            if not to_delete.get('time'):
                # if no time - it is overQueue appoint
                # and we need to delete it from table

                # get last time of that amb and if it is - return error
                max_time = execute_query(
                    const.select_max_time_for_overqueue,
                    {'action_id': to_delete['id']},
                    first=True
                ) or {}
                a_dt = datetime.strptime(
                    "{0} {1}".format(to_delete['setDate'], max_time['max_time']),
                    '%Y-%m-%d %H:%M:%S'
                )
                if a_dt < datetime.now():
                    get_db().rollback()
                    return make_response(jsonify(
                        error="Нельзя удалять прошедшую запись"
                    ), 400)

                execute_query(
                    """
                    DELETE FROM ActionProperty_Action
                    WHERE ActionProperty_Action.id = %(queue_id)s
                        AND ActionProperty_Action.`index` = %(index)s
                    """,
                    data
                )
                # after delete app-t we need to reorder indexes in other overQueue
                appoints = execute_query(
                    const.select_appointments_only_over_queue,
                    {
                        "id_doctor": to_delete['person_id'],
                        "search_date": to_delete['setDate'],
                    }
                )
                appoints = [a for a in appoints if a['ticketIndex'] > data['index']]
                for a in appoints:
                    new_index = a['ticketIndex'] - 1
                    execute_query(
                        """
                        UPDATE ActionProperty_Action AP_A
                        SET AP_A.`index` = %(new_index)s 
                        WHERE AP_A.id = %(queue_id)s
                            AND AP_A.`index` = %(index)s
                        """,
                        {
                            "new_index": new_index,
                            "queue_id": a['queueActionId'],
                            "index": a['ticketIndex'],
                        }
                    )
            else:

                # if time - we need only to clear value in AP_A table

                # if client try to delete past app-t - return error
                a_dt = datetime.strptime(
                    "{0} {1}".format(to_delete['setDate'], to_delete['time']),
                    '%Y-%m-%d %H:%M:%S'
                )
                if a_dt < datetime.now():
                    get_db().rollback()
                    return make_response(jsonify(
                        error="Нельзя удалять прошедшую запись"
                    ), 400)

                execute_query(
                    """
                    UPDATE ActionProperty_Action AP_A
                    SET AP_A.value = NULL 
                    WHERE AP_A.id = %(queue_id)s
                        AND AP_A.`index` = %(index)s
                    """,
                    data
                )
            execute_query(
                """
                DELETE FROM Action 
                WHERE Action.id = %(action_id)s
                """,
                {"action_id": to_delete["action_id"]}
            )
            execute_query(
                """
                DELETE FROM Event
                WHERE Event.id = %(event_id)s
                """,
                {"event_id": to_delete["event_id"]}
            )
            get_db().commit()
        except Exception as e:
            get_db().rollback()
            raise e

        return make_response(jsonify(info="ok"), 200)


class AppointmentsResource(Resource):
    schema = AppointmentsSchema(many=True)

    def post(self):
        # language=yaml
        """
        Запись к врачу
        ---
        tags:
        - timeTable
        description: >
            Позволяет записывать аппоинтменты по расписанию врача.

            Записи принимаются массивом, массивом же делается ответ для
            каждой записи.

            В этом же методе принимаются записи сверхочереди, для которых
            сохраняется вся логика (отличаем только по флагу isOverQueue).
        parameters:
        - in: body
          name: body
          description: >
            Параметры записи к врачу.


            * idPat - Идентификатор пациента.


            * personId - Идентификатор врача


            * date - Дата записи


            * time - Время записи. Обязательно, если OverQueue равняется false.
            Если isOverQueue=true, время записи игнорируется, даже
            если указан в параметрах


            * isOverQueue - Флаг, указывающи на то, является ли запись к врачу
            сверхочереди (true), или является плановой, т.е. по расписанию врача
            (false по умолчанию)

          schema:
            type: array
            items:
              type: object
              properties:
                idPat:
                  type: number
                  format: int32
                personId:
                  type: number
                  format: int32
                date:
                  type: string
                  example: YYYY-mm-dd
                time:
                  type: string
                  example: HH:MM:SS
                isOverQueue:
                  type: boolean
                  default: false
        responses:
          '200':
            description: Запрос успешен
            schema:
              type: array
              items:
                type: object
                properties:
                  'type':
                    type: string
                  message:
                    type: string
              example:
                - 'type': info
                  message: OK
                - 'type': error
                  message: Bag Request
                - 'type': error
                  message: Пациент имеет предстоящую запись к данному врачу/врачу этой специальности
                - 'type': error
                  message: Указаны недопустимые интервалы времени
                - 'type': error
                  message: Талон к врачу занят/заблокирован
                - 'type': error
                  message: Указан недопустимый идентификатор врача
                - 'type': error
                  message: Action with id=ID does not have property with name NAME
        """
        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error="Bad Request"), 400)
        response = []
        for elem in marsh_result.data:
            response.append(self.post_appointment(elem))
        return make_response(jsonify(response))

    def post_appointment(self, elem):

        id_pat = elem.get('id_pat')
        person_id = elem.get('person_id')
        date = elem.get('date')
        time = elem.get('time')
        is_over_queue = elem.get('is_over_queue')

        person_info = execute_query(
            """
            select p.org_id AS orgId, p.orgStructure_id, s.sex, s.age 
            from Person p 
                left join rbSpeciality s on s.id = p.speciality_id 
            where p.id=%(person_id)s
            """,
            {"person_id": person_id},
            first=True
        )
        amb_action_id = get_action_id('amb', person_id, date)
        req_cnt = execute_query(
            const.select_req_cnt,
            {
                "id_pat": id_pat,
                "person_id": person_id,
                "set_date": datetime.now().strftime('%Y-%m-%d')
            },
            first=True
        )
        if req_cnt['cnt']:
            # Уже есть запись к врачу этой специальности
            return {
                'type': 'error',
                'message': 'Пациент имеет предстоящую запись к данному врачу/врачу этой специальности'
            }
        else:
            if amb_action_id:
                # Если запись без очереди, создаем тикет
                # равный максимальному тикету в AP_A + 1
                if is_over_queue:
                    max_index = execute_query(
                        const.select_max_index,
                        {
                            "person_id": person_id,
                            "date": date
                        },
                        first=True
                    )['ticketIndex']
                    queue_index = max_index + 1
                else:
                    time_record = execute_query(
                        const.select_time_record,
                        {
                            "action_id": amb_action_id,
                            "time": time
                        },
                        first=True
                    )
                    if time_record is None:
                        # нет времени
                        return {
                            'type': 'error',
                            'message': 'Указаны недопустимые интервалы времени'
                        }
                    else:
                        queue_index = time_record['index']
                        queue = execute_query(
                            const.select_queue,
                            {"action_id": amb_action_id}
                        )
                        queue = result_proxy_to_array(queue)
                        if queue and queue[queue_index] is not None:
                            return {
                                'type': 'error',
                                'message': 'Талон к врачу занят/заблокирован'
                            }

                """
                Here custom connect because i need to commit only after 
                4 inserts, not after every one
                """
                get_db().begin()
                try:
                    office = get_action_property(
                        amb_action_id,
                        'ActionProperty_String',
                        'office'
                    )['value']
                    date_time = "{0} {1}".format(date, time)
                    event_type_id = execute_query(
                        "select id from EventType where code='queue'",
                        first=True
                    )['id']
                    action_type_id = execute_query(
                        "select id from ActionType where code='queue'",
                        first=True
                    )['id']
                    now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    event_record = {
                        'createDatetime': now,
                        'createPerson_id': _get_current_person(),
                        'eventType_id': event_type_id,
                        'org_id': person_info['orgId'],
                        'client_id': id_pat,
                        'setDate': date_time,
                        'isPrimary': 1,
                        # None insert
                        'modifyDatetime': now,
                        'externalId': "",
                        '`order`': 0,
                        'payStatus': 0,
                        'note': "",
                        'totalCost': 0,
                    }
                    event_id = execute_query(
                        """
                        insert into Event({0}) 
                        values({1})
                        """.format(
                            list_to_sql_array(event_record),
                            list_to_sql_array_with_wrap(event_record)
                        ),
                        event_record
                    )
                    action_record = {
                        'createDatetime': now,
                        'createPerson_id': _get_current_person(),
                        'actionType_id': action_type_id,
                        'event_id': event_id,
                        'directionDate': date_time,
                        'status': 1,
                        'person_id': person_id,
                        'setPerson_id': _get_current_person(),
                        'note': "Записано через РФ ЕГИСЗ",
                        'office': office,
                        # None insert
                        'modifyDatetime': now,
                        'plannedEndDate': now,
                        'amount': 0,
                        'payStatus': 0,
                        'account': 0,
                        'MKB': "",
                        'morphologyMKB': "",
                        'coordText': "",
                    }
                    queue_action_id = execute_query(
                        """
                        insert into Action({0}) 
                        values({1})
                        """.format(
                            list_to_sql_array(action_record),
                            list_to_sql_array_with_wrap(action_record),
                        ),
                        action_record
                    )
                    answer = set_action_property(
                        amb_action_id,
                        'queue',
                        'ActionProperty_Action',
                        queue_index,
                        queue_action_id
                    )
                    get_db().commit()
                    if answer and answer.get('type') == 'error':
                        return answer
                except Exception as e:
                    get_db().rollback()
                    raise e
                # Логика 12.07.2018
            else:
                # нет очереди
                return {
                    'type': 'error',
                    'message': 'Указан недопустимый идентификатор врача'
                }
        return {
            'type': 'info',
            'message': 'OK'
        }


class WaitingList(Resource):
    schema = WaitingListSchema()

    def post(self):
        # language=yaml
        """
        Запись в ЖОС
        ---
        tags:
        - timeTable
        description: >
          Записывает пациента в журнал отложенной записи (ЖОС)

          claim по умолчанию "нет_удобного_времени", и на данный момент не
          обязательный, хоть и ниже указано обратное
        parameters:
          - in: body
            name: body
            description: >
              Параметры записи в ЖОС


              * rule используется для указания, через сколько дней
              сегодняшней даты ставить запись

              Если rule не указан, ставит maxDate на +10 дней от текущей даты

              * claim - причина записи

              claim по умолчанию "нет\_удобного\_времени", и на данный момент не
              обязательный, хоть и ниже указано обратное
            schema:
              type: object
              properties:
                idPat:
                  type: number
                  format: int32
                personId:
                  type: number
                  format: int32
                specialityId:
                  type: number
                  format: int32
                rule:
                  type: string
                  example: 'YYYY-mm-dd'
                phone:
                  type: string
                  example: "+71234567890"
                claim:
                  type: string
        responses:
          '200':
            description: Запрос успешен
        """

        marsh_result = self.schema.load(request.get_json())
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        now = datetime.now().strftime("%Y-%m-%d %H-%M-%S")
        values = {
            'createDatetime': now,
            'createPerson_id': _get_current_person(),
            'modifyDatetime': now,
            'modifyPerson_id': _get_current_person(),
            'status_id': execute_query(
                "select id from rbDeferredQueueStatus where `code`='0'",
                first=True
            )['id'],
        }
        values.update(marsh_result.data)
        execute_query(
            """
            INSERT into DeferredQueue({0}) 
            VALUES({1})
            """.format(
                list_to_sql_array(values),
                list_to_sql_array_with_wrap(values)
            ),
            values
        )
        return make_response(jsonify(info='OK'), 200)


class QueueMove(Resource):
    schema_without_index = QueueMoveSchema(only=('idDoctor', 'searchDate'))
    schema = QueueMoveSchema()

    def get(self):
        # language=yaml
        """
        Список куда можно перенести запись
        ---
        tags:
        - timeTable
        description: >
          Возвращает список врачей, кому возможно перенести всю очередь
          в течении следующих 14 дней
        parameters:
        - in: query
          name: idDoctor
          type: string
          required: true
          description: Идентификатор врача
        - in: query
          name: searchDate
          type: string
          required: true
          description: Дата очереди, которую нужно перенести, формата YYYY-mm-dd
        responses:
          '200':
            description: >
              Запрос успешен
            schema:
              type: array
              items:
                type: object
                properties:
                  count:
                    type: number
                    format: int32
                  fio:
                    type: string
                    example: Иванов Иван Иванович
                  personId:
                    type: number
                    format: int32
                  specId:
                    type: number
                    format: int32
          '400':
            description: Bad Request
        """
        marsh_result = self.schema_without_index.load(request.args)
        if marsh_result.errors:
            return make_response(jsonify(error='Bad Request'), 400)
        data = marsh_result.data

        appoints_count = execute_query(
            const.select_index_count_with_value,
            data,
            first=True
        )
        speciality_id = execute_query(
            "SELECT p.speciality_id as id FROM Person p WHERE p.id = %(person_id)s",
            {"person_id": data['person_id']},
            first=True
        )['id']

        doctor_answer = execute_query(
            const.select_doctors_for_queue_move,
            {
                "specId": speciality_id,
                "today": data['search_date'],
                "maxDate": '2018-12-27',
                # "maxDate": (
                #         datetime.now() + timedelta(weeks=2)
                # ).strftime("%Y-%m-%d"),
                "min_count": appoints_count['count']
            }
        )
        return make_response(jsonify(doctor_answer))

    def post(self):
        # language=yaml
        """
        Перенос очереди врача
        ---
        tags:
        - timeTable
        description: >
         Переносит всю очередь одного врача с одного дня на запись к другому
         врачу (или тому же самому).
        parameters:
        - in: body
          name: body
          description: >
            * move_from - очередьб КОТОРУЮ нужно перенести

            * move_into - очередьб КУДА происходит перенос

              * index - опциональный параметр, указывающий, с какого
              индекса будет происходить запись очереди

          schema:
            type: object
            properties:
              move_from:
                type: object
                properties:
                  idDoctor:
                    type: number
                    format: int32
                  searchDate:
                    type: string
                    example: YYYY-mm-dd
              move_into:
                type: object
                properties:
                  idDoctor:
                    type: number
                    format: int32
                  searchDate:
                    type: string
                    example: YYYY-mm-dd
                  index:
                    type: number
                    format: int32
        responses:
          '200':
            description: Запрос успешен
          '400':
            description: Bad Request
        """
        data = request.get_json()
        marsh_result_from = self.schema_without_index.load(data.get('move_from'))
        marsh_result_into = self.schema.load(data.get('move_into'))
        if marsh_result_from.errors or marsh_result_into.errors:
            return make_response(jsonify(error='Bad Request'), 400)

        move_from = marsh_result_from.data
        move_into = marsh_result_into.data

        old_appoints = execute_query(
            const.select_appointments_with_value,
            {
                "person_id": move_from['person_id'],
                "search_date": move_from['search_date'],
            }
        )
        if not old_appoints:
            return make_response(
                jsonify(
                    error="На данное время отсутствует запись"
                ), 400)
        new_appoints = execute_query(
            const.select_appointments_without_value,
            {
                "person_id": move_into['person_id'],
                "search_date": move_into['search_date'],
            }
        )
        new_appoints = [a for a in new_appoints if a['ticketIndex'] >= move_into['index']]
        if len(new_appoints) < len(old_appoints):
            return make_response(jsonify(
                error="В расписании врача недостаточно места для переноса"
            ), 400)
        get_db().begin()
        try:
            for old, new in zip(old_appoints, new_appoints):
                execute_query(
                    """
                    UPDATE ActionProperty_Action AP_A
                    SET AP_A.value = %(value)s
                    WHERE AP_A.id = %(queue_id)s
                    AND AP_A.`index` = %(index)s
                    """,
                    {
                        "value": old['value'],
                        "queue_id": new['queueActionId'],
                        "index": new['ticketIndex'],
                    }
                )
                execute_query(
                    """
                    UPDATE ActionProperty_Action AP_A
                    SET AP_A.value = NULL
                    WHERE AP_A.id = %(queue_id)s
                        AND AP_A.`index` = %(index)s
                    """,
                    {
                        "queue_id": old['queueActionId'],
                        "index": old['ticketIndex'],
                    }
                )
                get_db().commit()
        except Exception as e:
            get_db().rollback()
            raise e
        return


def set_action_property(action_id, prop_name, table, index, value):
    action_type = execute_query(
        'select actionType_id AS id from Action where id=%(action_id)s',
        {"action_id": action_id},
        first=True
    )
    action_prop = execute_query(
        const.select_action_property,
        {
            "action_id": action_id,
            "action_type_id": action_type['id'],
            "prop_name": prop_name
        },
        first=True
    )
    if action_prop is None:
        action_prop_type = execute_query(
            const.select_action_property_type,
            {
                "action_type_id": action_type['id'],
                "prop_name": prop_name
            },
            first=True
        )

        if not action_prop_type:
            get_db().rollback()
            return {
                'type': 'error',
                'message': 'Action with id={0} does not have property with name {1}'.format(action_id, prop_name)
            }
        prop_record = {
            'createDatetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'createPerson_id': _get_current_person(),
            'action_id': action_id,
            'type_id': action_prop_type['id'],
            # None insert
            'modifyDatetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'norm': "",
        }
        action_prop = execute_query(
            """
            insert into ActionProperty({0}) 
            values({1})
            """.format(
                list_to_sql_array(prop_record),
                list_to_sql_array_with_wrap(prop_record)
            ),
            prop_record
        )
    prop_record = execute_query(
        """
        select * from {0} where id=%(action_prop_id)s and `index`=%(`index`)s
        """.format(table),
        {
            "action_prop_id": action_prop['id'],
            "`index`": index
        }
    )
    if prop_record:
        execute_query(
            """
            update {0} 
            set value=%(value)s 
            where id=%(action_prop_id)s 
            and `index`=%(index)s
            """.format(table), {
                "value": value,
                "action_prop_id": action_prop['id'],
                "index": index
            }
        )
    else:
        prop_record = {
            'id': action_prop['id'],
            '`index`': index,
            'value': value
        }
        execute_query(
            """
            insert into {0}({1}) 
             values({2})
             """.format(
                table,
                list_to_sql_array(prop_record),
                list_to_sql_array_with_wrap(prop_record)
            ),
            prop_record
        )


def get_action_id(action_type_code, person_id, date):
    action = execute_query(
        const.select_action,
        {
            "action_type_code": action_type_code,
            "person_id": person_id,
            "date": date
        },
        first=True
    )
    if action:
        return action['id']
    else:
        return None


def get_action_property(action_id, prop_table, prop_name):
    return execute_query(
        const.select_action_property_with_table_name.format(prop_table),
        {
            "action_id": action_id,
            "prop_name": prop_name
        },
        first=True
    )


def _get_current_person():
    # 1 - admin id
    return config['org_vars']['current_person']


def result_proxy_to_array(result_proxy):
    array = {}
    for row in result_proxy:
        array[row['ARRAY_KEY']] = row['value']
    return array
