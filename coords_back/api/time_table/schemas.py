from datetime import datetime, timedelta

from marshmallow import Schema, fields, validates, ValidationError, validates_schema, post_load

from base_schemas import SearchFromToBaseSchema
from database import execute_query


class DoctorSchema(SearchFromToBaseSchema):
    pass


class TimeTableSchema(Schema):
    idDoctor = fields.Int(
        required=True,
        attribute="id_doctor"
    )
    searchFrom = fields.Date(
        required=True,
        attribute="search_from",
        format='%Y-%m-%d'
    )
    searchTo = fields.Date(
        required=True,
        attribute="search_to",
        format='%Y-%m-%d'
    )

    @validates('idDoctor')
    def validate_doctor(self, idDoctor):
        doctor = execute_query("select id from Person WHERE id=%(id)s",
                               {'id': idDoctor},
                               first=True) or {}
        if not doctor:
            raise ValidationError('no such person')


class AppointmentSchema(Schema):
    queueId = fields.Int(
        required=True,
        attribute='queue_id'

    )
    ticketIndex = fields.Int(
        required=True,
        attribute='index'
    )
    # TODO validate queueId 'is_existing' for Action or ActionProp


class AppointmentsSchema(Schema):
    idPat = fields.Int(
        required=True,
        attribute='id_pat',
    )
    personId = fields.Int(
        required=True,
        attribute='person_id',
    )
    date = fields.Date(
        required=True,
        attribute='date',
        format='%Y-%m-%d'
    )
    time = fields.String(
        attribute='time',
        allow_none=True
    )
    # time = fields.Time(
    #    attribute='time',
    #    format='%H:%M'
    # )

    isOverQueue = fields.Boolean(
        attribute='is_over_queue',
        default=False
    )

    @validates_schema
    def validate_time(self, data):
        if not data.get('is_over_queue') and not data.get('time'):
            raise ValidationError('not overQueue appoints must include time')

    @post_load
    def time_if_over_queue(self, data):
        if data.get('is_over_queue'):
            # if overQueue - app-t wouldn't have any time
            data['time'] = datetime.now().date().strftime("%H-%M-%S")
        return data


class WaitingListSchema(Schema):
    idPat = fields.Int(
        required=True,
        attribute='client_id',
    )
    personId = fields.Int(
        required=True,
        attribute='person_id',
    )
    specialityId = fields.Int(
        required=True,
        attribute='speciality_id',
    )
    rule = fields.Date(
        format='%Y-%m-%d'
    )
    phone = fields.String(
        required=True,
    )
    claim = fields.String(
        default='нет_удобного_времени',
    )

    @validates('personId')
    def validate_doctor(self, idDoctor):
        doctor = execute_query(
            "select id from Person WHERE id=%(id)s",
            {'id': idDoctor},
            first=True) or {}
        if not doctor:
            raise ValidationError('no such person')

    @validates('idPat')
    def validate_pat_id(self, pat_id):
        client = execute_query(
            "SELECT id FROM Client WHERE id=%(pat_id)s",
            {"pat_id": pat_id}
        ) or {}
        if not client:
            raise ValidationError('no such client')

    @validates('specialityId')
    def validate_speciality(self, speciality_id):
        spec = execute_query(
            "SELECT id FROM rbSpeciality WHERE id=%(speciality_id)s",
            {"speciality_id": speciality_id}
        )
        if not spec:
            raise ValidationError('no such spec')

    @post_load
    def return_waiting_list_record(self, data):
        rule = data.pop('rule', None)
        phone = data.pop('phone')
        claim = data.pop('claim', 'нет_удобного_времени')
        if rule:
            data['maxDate'] = rule
        else:
            data['maxDate'] = (
                    datetime.now().date() + timedelta(days=10)
            ).strftime("%Y-%m-%d %H-%M-%S")
        data['comment'] = "Телефон: {0}, причина: {1}".format(phone, claim)
        return data


class QueueMoveSchema(Schema):
    idDoctor = fields.Int(
        required=True,
        attribute='person_id'
    )
    searchDate = fields.Date(
        required=True,
        attribute='search_date',
        format='%Y-%m-%d'
    )
    index = fields.Int(
        required=True
    )

    @validates('idDoctor')
    def validate_doctor(self, idDoctor):
        doctor = execute_query("select id from Person WHERE id=%(id)s",
                               {'id': idDoctor},
                               first=True)
        if not doctor:
            raise ValidationError('no such person')
