from datetime import datetime, timedelta

from marshmallow import Schema, fields, ValidationError, validates_schema


# TODO swap into marshmallow 3 or add 'load_from', according with name changes


# base schemas

class SearchFromToBaseSchema(Schema):
    searchFrom = fields.Date(
        format='%Y-%m-%d',
        attribute='search_from'
    )
    searchTo = fields.Date(
        format='%Y-%m-%d',
        attribute='search_to'
    )

    @validates_schema()
    def is_both(self, data):
        if not data.get('search_from') and not data.get('search_to'):
            data['search_from'] = datetime.now().date()
            data['search_to'] = data['search_from'] + timedelta(days=14)
        elif not data.get('search_to') or not data.get('search_from'):
            raise ValidationError('Must contain two dates or not at all')


# TODO Create schema for validation person, client and other stuff