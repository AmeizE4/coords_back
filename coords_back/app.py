# -*- coding: utf-8 -*-
from flask import Flask
from flasgger import Swagger
from flask_cors import CORS

from config import config


def create_app():
    """
    factory method for flask-app
    """
    app = Flask(__name__)
    app.config['ENV'] = config['flask']['env']
    app.config['DEBUG'] = config['flask']['debug']

    with app.app_context():
        from api import api_blueprint
        app.register_blueprint(api_blueprint)

    cors = CORS(app)

    app.config['SWAGGER'] = {
        'title': 'API регистратора и врача на дом',
        "termsOfService": None,
        "description": None,
        "version": None
    }
    swagger = Swagger(app)

    return app
