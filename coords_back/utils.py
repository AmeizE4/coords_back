"""
This module provide utility functions that are used in this project.
"""


def list_to_sql_array(iter_):
    return ', '.join(map(str, iter_))


def list_to_sql_array_with_wrap(iter_):
    return ', '.join(map(_wrap, iter_))


def list_to_sql_array_with_equal_wrap(iter_):
    return ', '.join(map(lambda a: '{}={}'.format(a, _wrap(a)), iter_))


def _wrap(elem):
    return '%({})s'.format(elem)
