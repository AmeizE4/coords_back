"""
Main module, from which is started flask app.
Must be in project folder, but cant deal with import problems.
"""

from app import create_app
from config import config

app = create_app()

if __name__ == '__main__':
    app.run(
        host=config['flask']['host'],
        port=config['flask']['port']
    )
