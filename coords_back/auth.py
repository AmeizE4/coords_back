from functools import wraps

import flask
import flask_restful
from flask_restful import Resource

from database import execute_query


def auth_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        auth_header = flask.request.headers.get('Authorization', '')
        parts = auth_header.split(' ')
        if len(parts) != 2:
            flask_restful.abort(401)
        if parts[0] != 'Bearer':
            flask_restful.abort(401)

        token = parts[1]

        session = execute_query(
            """
            SELECT S.user_id, S.person_id
            FROM backend_logger.Session S
                INNER JOIN User U ON S.user_id = U.id
                INNER JOIN Person P ON S.person_id = P.id
            WHERE S.token = %(token)s
                  AND S.expireDatetime > NOW()
            """,
            {"token": token},
            first=True
        )

        if session:
            user = execute_query(
                "SELECT * FROM User WHERE id=%(user_id)s",
                {"user_id": session['user_id']}
            )
            flask.g.user = user
            person = execute_query(
                "SELECT * FROM Person WHERE id=%(person_id)s",
                {"person_id": session['person_id']}
            )
            flask.g.person = person
            return func(*args, **kwargs)
        else:
            flask_restful.abort(401)

    return wrapper


class ClosedResource(Resource):
    method_decorators = [auth_required]
