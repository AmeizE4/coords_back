# -*- coding: utf-8 -*-

"""
Contains tools to work with PyMySQL.
"""

import pymysql.cursors
from pymysql.err import OperationalError
from flask import g, current_app

from config import config

host = config['mysql']['host']
port = config['mysql']['port']
user = config['mysql']['user']
password = config['mysql']['password']
db = config['mysql']['db']
charset = config['mysql']['charset']


def connect():
    return pymysql.connect(
        host=host,
        port=port,
        user=user,
        password=password,
        db=db,
        charset=charset
    )


def do_connect():
    db = connect()
    db.autocommit(True)
    return db


@current_app.teardown_appcontext
def teardown_db(exception):
    # TODO непроисходит, если запрос закончился ошибкой.
    #  Отсюда возможно и возникала ошибка lost connection.
    #  У flask есть несколько похожих декораторов для обработки постзапроса :
    #  teardown_appcontext (используется здесь),
    #  after_request(после каждого реквеста стабильно, передает в него ответ),
    #  и teardown_request.
    #  нужно определиться какой использовать и как они работают.
    db = getattr(g, '_db', None)
    if db is not None:
        db.close()


def get_db():
    db = getattr(g, '_db', None)
    if db is None:
        db = g._db = do_connect()
    return db


def execute_query(query, params=(), first=False):
    try:
        with get_db().cursor(pymysql.cursors.DictCursor) as cursor:
            print(query, params)
            cursor.execute(query, params)
            if first:
                # FIXME first param need to painless move from old db code, but steel bad move and need to be removed
                data = cursor.fetchone()
                return data
            data = cursor.fetchall()
            # TODO: Время от времени lastrowid не равно нулю даже если у нас SELECT
            # TODO: У меня не воспроизводится. Воспроизводилось на Ubuntu Паши
            if cursor.lastrowid and ('INSERT' in query.upper()):
                data = cursor.lastrowid
        return data
    except OperationalError as e:  # lost connection. Это нормально. Нужно реконнектнуться
        delattr(g, '_db')
        with get_db().cursor(pymysql.cursors.DictCursor) as cursor:
            cursor.execute(query, params)
            data = cursor.fetchall()
            if cursor.lastrowid and ('INSERT' in query.upper()):
                data = cursor.lastrowid
        return data
