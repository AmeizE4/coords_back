"""
Contain all SQL queries, which used by this API.

SQL queries use doubled symbol '%%' for avoiding mistakes.

For example, DATE_FORMAT(date_field, format) was used as
    DATE_FORMAT(Event.execDate, '%Y-%m-%d')
in console, but in python will be with double '%'
    DATE_FORMAT(Event.execDate, '%%Y-%%m-%%d')
"""

select_doctors = """
SELECT p.id AS `personId`,
       CONCAT(p.lastName, ' ', p.firstName, ' ', p.patrName) AS `fio`,
       rS.id AS `specId`,
       rS.name AS `specName`,
       OS.id AS `clinicId`,
       OS.name AS `uch`,
       DATE_FORMAT(MIN(e.setDate), '%%Y-%%m-%%d') as 'minDate',
       DATE_FORMAT(MAX(e.setDate), '%%Y-%%m-%%d') as 'maxDate',
       SUM(IF(a1.id IS NULL,1,0)) AS 'available'
FROM Person p
  INNER JOIN rbSpeciality rS on p.speciality_id = rS.id AND NOT ISNULL(rS.id)
  LEFT JOIN OrgStructure OS on p.orgStructure_id = OS.id

  INNER JOIN Event e ON e.setPerson_id = p.id AND e.deleted = 0
  INNER JOIN EventType et ON et.id = e.eventType_id AND et.code = '0'
  INNER JOIN Action a ON a.event_id = e.id AND a.deleted = 0
  INNER JOIN ActionType apt ON a.actionType_id = apt.id AND apt.code = 'amb'

  LEFT JOIN ActionProperty AP_queue on a.id = AP_queue.action_id AND AP_queue.action_id = a.id
  INNER JOIN ActionPropertyType APT_queue on AP_queue.type_id = APT_queue.id AND APT_queue.name='queue'
  INNER JOIN ActionProperty_Action AP_A_queue on AP_queue.id = AP_A_queue.id AND AP_A_queue.value IS NULL

  LEFT JOIN Action a1 ON AP_A_queue.value = a1.id AND a1.deleted = 0
WHERE 1=1
    AND p.orgStructure_id in %(orgStructs)s
    AND e.setDate >= %(search_from)s
    AND e.setDate <= %(search_to)s

    AND (p.lastAccessibleTimelineDate IS NULL OR p.lastAccessibleTimelineDate = '0000-00-00' OR DATE(e.setDate)<=p.lastAccessibleTimelineDate)
    AND (p.timelineAccessibleDays IS NULL OR p.timelineAccessibleDays <= 0 OR DATE(e.setDate)<=ADDDATE(CURRENT_DATE(), p.timelineAccessibleDays))
    AND e.id NOT IN (SELECT Event.id FROM Event
              LEFT JOIN Action ON Action.event_id = Event.id
              INNER JOIN ActionProperty ON ActionProperty.action_id = Action.id
              INNER JOIN ActionProperty_rbReasonOfAbsence ON ActionProperty_rbReasonOfAbsence.id = ActionProperty.id
              LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
             WHERE ActionType.code = 'timeLine') -- remove Events in which there is a reason for the absence
GROUP BY fio;
"""

select_req_cnt = """
select count(QueueAction.id) as cnt
from Action AS QueueAction
       LEFT JOIN ActionType AS QueueActionType ON QueueActionType.id = QueueAction.actionType_id
       LEFT JOIN Person AS QueuePerson ON QueuePerson.id = QueueAction.person_id
       LEFT JOIN Event AS QueueEvent ON QueueEvent.id = QueueAction.event_id
       LEFT JOIN EventType AS QueueEventType ON QueueEventType.id = QueueEvent.eventType_id
       LEFT JOIN ActionProperty_Action ON ActionProperty_Action.value = QueueAction.id
       LEFT JOIN ActionProperty ON ActionProperty.id = ActionProperty_Action.id
       LEFT JOIN Action ON Action.id = ActionProperty.action_id
       LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
       LEFT JOIN Event ON Event.id = Action.event_id
       LEFT JOIN EventType ON EventType.id = Event.eventType_id
       LEFT JOIN Person ON Person.speciality_id = QueuePerson.speciality_id
where QueueAction.deleted = 0
  AND QueueActionType.code = 'queue'
  AND QueueEvent.deleted = 0
  AND QueueEventType.code = 'queue'
  AND Action.deleted = 0
  AND ActionType.code = 'amb'
  AND Event.deleted = 0
  AND EventType.code = '0'
  AND QueueEvent.client_id = %(id_pat)s
  AND Person.id = %(person_id)s
  AND QueueEvent.setDate >= %(set_date)s      
"""

select_history = """
select QueueEvent.client_id                                         AS `clientId`,
       DATE_FORMAT(DATE(QueueEvent.setDate), '%%Y-%%m-%%d')         AS `date`,
       TIME_FORMAT(TIME(ActionProperty_Time.value), '%%T')          AS `time`,
       ActionProperty_Action.`index`                                AS `index`,
       QueueAction.person_id                                        AS `personId`,
       QueueAction.note                                             AS `note`,
       QueueAction.id                                               AS `queueActionId`,
       QueueAction.createPerson_id                                  AS `enqueuePersonId`,
       QueueAction.createDatetime                                   AS `enqueueDateTime`,
       ActionProperty.action_id                                     AS `ambActionId`,
       ActionProperty.id                                            AS `ambActionPropId`,
       Person.id                                                    as docId,
       Person.firstName                                             as docFirstName,
       Person.lastName                                              as docLastName,
       Person.patrName                                              as docPatrName,
       rbSpeciality.name                                            as specName,
       rbSpeciality.id                                              as specId
from Action AS QueueAction
       LEFT JOIN ActionType AS QueueActionType ON QueueActionType.id = QueueAction.actionType_id
       LEFT JOIN Event AS QueueEvent ON QueueEvent.id = QueueAction.event_id
       LEFT JOIN EventType AS QueueEventType ON QueueEventType.id = QueueEvent.eventType_id
       LEFT JOIN ActionProperty_Action ON ActionProperty_Action.value = QueueAction.id
       LEFT JOIN ActionProperty ON ActionProperty.id = ActionProperty_Action.id
       LEFT JOIN Action ON Action.id = ActionProperty.action_id
       LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
       LEFT JOIN ActionPropertyType AS APTTime ON APTTime.actionType_id = ActionType.id AND APTTime.name = 'times'
       LEFT JOIN ActionProperty AS APTime ON APTime.type_id = APTTime.id AND APTime.action_id = Action.id
       LEFT JOIN ActionProperty_Time
         ON ActionProperty_Time.id = APTime.id AND ActionProperty_Time.`index` = ActionProperty_Action.`index`
       LEFT JOIN Event ON Event.id = Action.event_id
       LEFT JOIN EventType ON EventType.id = Event.eventType_id
       LEFT JOIN Person ON QueueAction.person_id = Person.id
       LEFT JOIN rbSpeciality ON Person.speciality_id = rbSpeciality.id
where QueueAction.deleted = 0
  AND QueueActionType.code = 'queue'
  AND QueueEvent.deleted = 0
  AND QueueEventType.code = 'queue'
  AND Action.deleted = 0
  AND ActionType.code = 'amb'
  AND Event.deleted = 0
  AND EventType.code = '0'
  AND QueueEvent.client_id = %(pat_id)s
  -- AND QueueEvent.setDate > ?
ORDER BY date DESC       
"""

select_dates = """
SELECT DISTINCT DATE_FORMAT(DATE(Event.execDate), '%%Y-%%m-%%d') AS date, 
                p.lastName AS personName
FROM Event
       INNER JOIN Action ON Event.id = Action.event_id AND Action.deleted = 0
       INNER JOIN ActionType
         ON Action.actionType_id = ActionType.id AND ActionType.deleted = 0 AND ActionType.code = 'amb'
       LEFT JOIN Person p ON p.id = Event.execPerson_id
       LEFT JOIN rbSpeciality spec ON p.speciality_id = spec.id
WHERE Event.deleted = 0
  AND p.id = %(id_doctor)s
  AND Event.setDate between %(search_from)s and %(search_to)s
  
  AND (p.lastAccessibleTimelineDate IS NULL OR p.lastAccessibleTimelineDate = '0000-00-00' OR
       DATE(Event.setDate) <= p.lastAccessibleTimelineDate)
  AND (p.timelineAccessibleDays IS NULL OR p.timelineAccessibleDays <= 0 OR
       DATE(Event.setDate) <= ADDDATE(CURRENT_DATE(), p.timelineAccessibleDays))
  AND EXISTS(
        SELECT *
        FROM Action a
               INNER JOIN ActionType atp ON a.actionType_id = atp.id
               LEFT JOIN ActionPropertyType aptq1 ON aptq1.actionType_id = a.actionType_id AND aptq1.name = 'queue'
               LEFT JOIN ActionProperty apq1 ON apq1.action_id = a.id AND apq1.type_id = aptq1.id AND apq1.deleted = 0
               LEFT JOIN ActionProperty_Action apqa1 ON apqa1.id = apq1.id AND apqa1.`index` IS NOT NULL 
               --
               LEFT JOIN ActionPropertyType aptype_times
                 ON aptype_times.actionType_id = atp.id AND aptype_times.name LIKE 'times'
               LEFT JOIN ActionProperty ap_times
                 ON ap_times.action_id = a.id AND ap_times.type_id = aptype_times.id AND ap_times.deleted = 0
               LEFT JOIN ActionProperty_Time aptime ON aptime.id = ap_times.id AND aptime.`index` = apqa1.`index`
        WHERE a.id = Action.id
          AND aptime.value IS NOT NULL
          )
"""

select_all_appointments = """
SELECT p.id                                  As person_id,
       p.name                                As personName,
       p.orgStructure_id                     As clinicId,
       APA_queue.id                          AS queueActionId,
       APA_queue.index                       AS ticketIndex,
       TIME_FORMAT(AP_T_times.value, '%%T')  AS ticketTime,
       APA_queue.value                       AS value,
       Client.id                             AS client_id,
       Client.lastName                       AS client_last_name,
       Client.firstName                      AS client_first_name,
       Client.patrName                       AS client_patr_name,
       Event.execDate                        AS date

FROM Event
       INNER JOIN Action ON Event.id = Action.event_id AND Action.deleted = 0
       INNER JOIN ActionType ON Action.actionType_id = ActionType.id AND ActionType.code = 'amb'
       -- get indexes from AP_A
       INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id AND APT_queue.name = 'queue'
       INNER JOIN ActionProperty AP_queue ON Action.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
       INNER JOIN ActionProperty_Action APA_queue ON AP_queue.id = APA_queue.id
       -- get times from AP_T
       INNER JOIN ActionPropertyType APT_times ON ActionType.id = APT_times.actionType_id AND APT_times.name = 'times'
       INNER JOIN ActionProperty AP_times ON Action.id = AP_times.action_id AND APT_times.id = AP_times.type_id
       LEFT JOIN ActionProperty_Time AP_T_times ON AP_times.id = AP_T_times.id AND AP_T_times.index = APA_queue.index
       -- get office info 
       LEFT JOIN ActionPropertyType APT_office
         ON ActionType.id = APT_office.actionType_id AND APT_office.name = 'office'
       LEFT JOIN ActionProperty AP_office ON Action.id = AP_office.action_id AND APT_office.id = AP_office.type_id
       LEFT JOIN ActionProperty_String AP_S_office ON AP_office.id = AP_S_office.id
       -- get person info
       INNER JOIN vrbPerson p ON p.id = Event.execPerson_id
       LEFT JOIN rbSpeciality spec ON p.speciality_id = spec.id
       -- get client info
       LEFT JOIN Action Action_queue ON APA_queue.value = Action_queue.id
       LEFT JOIN Event Event_queue ON Action_queue.event_id = Event_queue.id
       LEFT JOIN Client ON Event_queue.client_id = Client.id
WHERE Event.deleted = 0
  AND p.id = %(id_doctor)s
  AND DATE(Event.execDate) IN ({})
"""

select_appointments_only_over_queue = (
    """
SELECT p.id                                  As person_id,
       p.name                                As personName,
       p.orgStructure_id                     As clinicId,
       APA_queue.id                          AS queueActionId,
       APA_queue.index                       AS ticketIndex,
       TIME_FORMAT(AP_T_times.value, '%%T')  AS ticketTime,
       APA_queue.value                       AS value,
       Client.id                             AS client_id,
       Client.lastName                       AS client_last_name,
       Client.firstName                      AS client_first_name,
       Client.patrName                       AS client_patr_name,
       Event.execDate                        AS date

FROM Event
       INNER JOIN Action ON Event.id = Action.event_id AND Action.deleted = 0
       INNER JOIN ActionType ON Action.actionType_id = ActionType.id AND ActionType.code = 'amb'
       -- get indexes from AP_A
       INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id AND APT_queue.name = 'queue'
       INNER JOIN ActionProperty AP_queue ON Action.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
       INNER JOIN ActionProperty_Action APA_queue ON AP_queue.id = APA_queue.id
       -- get times from AP_T
       INNER JOIN ActionPropertyType APT_times ON ActionType.id = APT_times.actionType_id AND APT_times.name = 'times'
       INNER JOIN ActionProperty AP_times ON Action.id = AP_times.action_id AND APT_times.id = AP_times.type_id
       LEFT JOIN ActionProperty_Time AP_T_times ON AP_times.id = AP_T_times.id AND AP_T_times.index = APA_queue.index
       -- get office info 
       LEFT JOIN ActionPropertyType APT_office
         ON ActionType.id = APT_office.actionType_id AND APT_office.name = 'office'
       LEFT JOIN ActionProperty AP_office ON Action.id = AP_office.action_id AND APT_office.id = AP_office.type_id
       LEFT JOIN ActionProperty_String AP_S_office ON AP_office.id = AP_S_office.id
       -- get person info
       INNER JOIN vrbPerson p ON p.id = Event.execPerson_id
       LEFT JOIN rbSpeciality spec ON p.speciality_id = spec.id
       -- get client info
       LEFT JOIN Action Action_queue ON APA_queue.value = Action_queue.id
       LEFT JOIN Event Event_queue ON Action_queue.event_id = Event_queue.id
       LEFT JOIN Client ON Event_queue.client_id = Client.id
WHERE Event.deleted = 0
  AND p.id = %(id_doctor)s
  AND DATE(Event.execDate) IN (%(search_date)s)
  AND AP_T_times.id IS NULL 
"""
)

select_all_appointments_without_over_queue = """
SELECT APA_queue.id      AS queueActionId,
       APA_queue.index   AS ticketIndex,
       AP_T_times.value  AS ticketTime,
       APA_queue.value   AS value

FROM Event
       INNER JOIN Action ON Event.id = Action.event_id
       INNER JOIN ActionType ON Action.actionType_id = ActionType.id
       INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id
       INNER JOIN ActionProperty AP_queue ON Action.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
       INNER JOIN ActionProperty_Action APA_queue ON AP_queue.id = APA_queue.id
       INNER JOIN ActionPropertyType APT_times ON ActionType.id = APT_times.actionType_id
       INNER JOIN ActionProperty AP_times ON Action.id = AP_times.action_id AND APT_times.id = AP_times.type_id
       INNER JOIN ActionProperty_Time AP_T_times ON AP_times.id = AP_T_times.id AND AP_T_times.index = APA_queue.index
       INNER JOIN ActionPropertyType APT_office
         ON ActionType.id = APT_office.actionType_id AND APT_office.name = 'office'
       LEFT JOIN ActionProperty AP_office ON Action.id = AP_office.action_id AND APT_office.id = AP_office.type_id
       LEFT JOIN ActionProperty_String AP_S_office ON AP_office.id = AP_S_office.id
       INNER JOIN vrbPerson p ON p.id = Event.execPerson_id
WHERE Event.deleted = 0
  AND Action.deleted = 0
  AND ActionType.code IN ('amb')
  AND APT_queue.name = 'queue'
  AND APT_times.name = 'times'
  AND p.id in (%(person_id)s)
  AND DATE(Event.execDate) IN (%(search_date)s)
"""

select_appointments_with_value = (
        select_all_appointments_without_over_queue
        + """AND APA_queue.value IS NOT NULL """
)

select_appointments_without_value = (
        select_all_appointments_without_over_queue
        + """AND APA_queue.value IS NULL """
)

select_action_property = """
select ActionProperty.id 
from ActionProperty
        LEFT JOIN ActionPropertyType ON ActionPropertyType.id = ActionProperty.type_id
where
  ActionProperty.action_id = %(action_id)s
  AND ActionProperty.deleted = 0
  AND ActionPropertyType.actionType_id = %(action_type_id)s
  AND ActionPropertyType.name = %(prop_name)s
"""

select_action_property_type = """
select ActionPropertyType.id 
from ActionPropertyType
        LEFT JOIN ActionType ON ActionType.id = ActionPropertyType.actionType_id
where
  ActionType.id = %(action_type_id)s
  AND ActionPropertyType.name = %(prop_name)s
  AND ActionPropertyType.deleted = 0
"""

select_action = """
select Action.id
from Action
        LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
        LEFT JOIN Event ON Event.id = Action.event_id
        LEFT JOIN EventType ON EventType.id = Event.eventType_id
        LEFT JOIN Person ON Person.id = Event.setPerson_id
where
  `Event`.`deleted`=0 AND `Action`.`deleted`=0 AND `EventType`.`code` = '0'
  AND `ActionType`.`code` = %(action_type_code)s
  AND `Event`.`setPerson_id` = %(person_id)s
  AND `Event`.`setDate` = %(date)s
  AND (Person.lastAccessibleTimelineDate IS NULL OR Person.lastAccessibleTimelineDate = '0000-00-00' OR DATE(Event.setDate)<=Person.lastAccessibleTimelineDate)
  AND (Person.timelineAccessibleDays IS NULL OR Person.timelineAccessibleDays <= 0 OR DATE(Event.setDate)<=ADDDATE(CURRENT_DATE(), Person.timelineAccessibleDays))
"""

select_action_property_with_table_name = """
select {0}.value 
from ActionProperty
  LEFT JOIN {0} ON {0}.id = ActionProperty.id
  LEFT JOIN ActionPropertyType ON ActionPropertyType.id = ActionProperty.type_id
where
  ActionProperty.action_id = %(action_id)s
  AND ActionProperty.deleted = 0
  AND ActionPropertyType.name = %(prop_name)s
  """

select_max_index = """
SELECT Event.id        AS queueEvent_id,
     p.id              As person_id,
     p.name            As personName,
     p.orgStructure_id As clinicId,
     MAX(APA_queue.index)  AS ticketIndex,
     APA_queue.value   AS value
FROM Event
     INNER JOIN Action ON Event.id = Action.event_id
     INNER JOIN ActionType ON Action.actionType_id = ActionType.id
     INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id
     INNER JOIN ActionProperty AP_queue ON Action.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
     INNER JOIN ActionProperty_Action APA_queue ON AP_queue.id = APA_queue.id
     INNER JOIN ActionPropertyType APT_times ON ActionType.id = APT_times.actionType_id
     INNER JOIN ActionProperty AP_times ON Action.id = AP_times.action_id AND APT_times.id = AP_times.type_id
     LEFT JOIN ActionProperty_Time AP_T_times ON AP_times.id = AP_T_times.id AND AP_T_times.index = APA_queue.index
     LEFT JOIN ActionPropertyType APT_office
       ON ActionType.id = APT_office.actionType_id AND APT_office.name = 'office'
     LEFT JOIN ActionProperty AP_office ON Action.id = AP_office.action_id AND APT_office.id = AP_office.type_id
     LEFT JOIN ActionProperty_String AP_S_office ON AP_office.id = AP_S_office.id
     INNER JOIN vrbPerson p ON p.id = Event.execPerson_id
     LEFT JOIN rbSpeciality spec ON p.speciality_id = spec.id
WHERE Event.deleted = 0
AND Action.deleted = 0
AND ActionType.code IN ('amb')
AND APT_queue.name = 'queue'
AND APT_times.name = 'times'
AND p.id in (%(person_id)s)
AND DATE(Event.execDate) IN (%(date)s)
GROUP BY event_id
"""

select_index_count_with_value = """
SELECT Event.id        AS queueEvent_id,
     p.id              As person_id,
     p.name            As personName,
     p.orgStructure_id As clinicId,
     count(*)  AS count
FROM Event
     INNER JOIN Action ON Event.id = Action.event_id
     INNER JOIN ActionType ON Action.actionType_id = ActionType.id
     INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id
     INNER JOIN ActionProperty AP_queue ON Action.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
     INNER JOIN ActionProperty_Action APA_queue ON AP_queue.id = APA_queue.id
     INNER JOIN ActionPropertyType APT_times ON ActionType.id = APT_times.actionType_id
     INNER JOIN ActionProperty AP_times ON Action.id = AP_times.action_id AND APT_times.id = AP_times.type_id
     LEFT JOIN ActionProperty_Time AP_T_times ON AP_times.id = AP_T_times.id AND AP_T_times.index = APA_queue.index
     LEFT JOIN ActionPropertyType APT_office
       ON ActionType.id = APT_office.actionType_id AND APT_office.name = 'office'
     LEFT JOIN ActionProperty AP_office ON Action.id = AP_office.action_id AND APT_office.id = AP_office.type_id
     LEFT JOIN ActionProperty_String AP_S_office ON AP_office.id = AP_S_office.id
     INNER JOIN vrbPerson p ON p.id = Event.execPerson_id
     LEFT JOIN rbSpeciality spec ON p.speciality_id = spec.id
WHERE Event.deleted = 0
AND Action.deleted = 0
AND ActionType.code IN ('amb')
AND APT_queue.name = 'queue'
AND APT_times.name = 'times'
AND p.id in (%(person_id)s)
AND DATE(Event.execDate) IN (%(search_date)s)
AND APA_queue.value IS NOT NULL
"""

select_time_record = """
select ActionProperty_Time.index 
from ActionProperty
    LEFT JOIN ActionProperty_Time ON ActionProperty_Time.id = ActionProperty.id
    LEFT JOIN ActionPropertyType ON ActionPropertyType.id = ActionProperty.type_id
where
    ActionProperty.action_id = %(action_id)s
    AND ActionPropertyType.name = 'times'
    AND ActionProperty_Time.value = TIME(%(time)s)     
    """

select_queue = """
select ActionProperty_Action.index as ARRAY_KEY, ActionProperty_Action.value
from Action
       LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
       LEFT JOIN ActionPropertyType ON ActionPropertyType.actionType_id = ActionType.id
       LEFT JOIN ActionProperty
         ON ActionProperty.action_id = Action.id AND ActionProperty.type_id = ActionPropertyType.id
       LEFT JOIN ActionProperty_Action ON ActionProperty_Action.id = ActionProperty.id
where Action.id = %(action_id)s
  AND ActionPropertyType.deleted = 0
  AND ActionProperty.deleted = 0
  AND ActionProperty.id IS NOT NULL
  AND ActionPropertyType.name = 'queue'
order by ActionProperty_Action.id, ActionProperty_Action.index
"""

select_doctors_for_queue_move = """
SELECT s.personId,
       s.fio,
       s.specId,
       MAX(count) as count
FROM (SELECT
             s.id                                                  AS `specId`,
             s.name                                                AS `specName`,
             p.id                                                  AS `personId`,
             e.setDate                                             AS `setDate`,
             CONCAT(p.lastName, ' ', p.firstName, ' ', p.patrName) AS `fio`,
             Count(*)                                            AS 'count'
      FROM Person p
             LEFT JOIN rbSpeciality s ON p.speciality_id = s.id
             LEFT JOIN Event e ON e.setPerson_id = p.id
             LEFT JOIN EventType et ON et.id = e.eventType_id
             LEFT JOIN Action a ON a.event_id = e.id
             LEFT JOIN ActionType ON ActionType.id = a.actionType_id --
             LEFT JOIN ActionPropertyType APT ON APT.actionType_id = a.actionType_id AND APT.name = 'queue'
             LEFT JOIN ActionProperty ON ActionProperty.action_id = a.id AND ActionProperty.type_id = APT.id AND ActionProperty.deleted = 0
             LEFT JOIN ActionProperty_Action AP_A ON AP_A.id = ActionProperty.id AND AP_A.`index` IS NOT NULL
             LEFT JOIN ActionPropertyType aptype_times
               ON aptype_times.actionType_id = ActionType.id AND aptype_times.name LIKE 'times'
             LEFT JOIN ActionProperty ap_times
               ON ap_times.action_id = a.id AND ap_times.type_id = aptype_times.id AND ap_times.deleted = 0
             LEFT JOIN ActionProperty_Time AP_T ON AP_T.id = ap_times.id AND AP_T.`index` = AP_A.`index`
             LEFT JOIN ActionProperty APESS ON APESS.action_id = a.id AND APESS.type_id = (select id
                                                                                           from ActionPropertyType
                                                                                           where name = 'notExternalSystems')
             LEFT JOIN ActionProperty_Integer APESSI ON APESS.id = APESSI.id AND APESSI.index = AP_T.index

      WHERE IF(%(specId)s IS NOT NULL, p.speciality_id IN (%(specId)s), p.speciality_id IS NOT NULL)
        AND e.setDate >= %(today)s
        AND e.setDate <= %(maxDate)s
        -- AND COUNT(*) >= %(min_count)s
        AND NOT ISNULL(s.id)
        AND e.deleted = 0
        AND a.deleted = 0
          -- FIXME Почему этот участок кода закомментирован?
          -- AND a1.deleted = 0
        AND et.code = '0'
        AND ActionType.code = 'amb'
        AND AP_T.value IS NOT NULL  -- has planned time for appointment
        AND AP_A.value IS NULL
        AND (p.lastAccessibleTimelineDate IS NULL OR p.lastAccessibleTimelineDate = '0000-00-00' OR
             DATE(e.setDate) <= p.lastAccessibleTimelineDate)
        AND (p.timelineAccessibleDays IS NULL OR p.timelineAccessibleDays <= 0 OR
             DATE(e.setDate) <= ADDDATE(CURRENT_DATE(), p.timelineAccessibleDays))
        AND e.id NOT IN (SELECT Event.id
                         FROM Event
                                LEFT JOIN Action ON Action.event_id = Event.id
                                INNER JOIN ActionProperty ON ActionProperty.action_id = Action.id
                                INNER JOIN ActionProperty_rbReasonOfAbsence
                                  ON ActionProperty_rbReasonOfAbsence.id = ActionProperty.id
                                LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
                         WHERE ActionType.code = 'timeLine') -- remove Events in which there is a reason for the absence
      GROUP BY fio, setDate
      ORDER BY s.name, `fio`) s
       LEFT JOIN PersonPrerecordQuota pq ON pq.person_id = s.personId
WHERE s.count >= %(min_count)s
GROUP BY s.specId, s.personId
"""

select_AP_A_for_appoint_res = """
SELECT AP_A_queue.id       AS id,
       AP_A_queue.value    AS value,
       Action_queue.id     AS action_id,
       Event_queue.id      AS event_id,
       AP_T_times.value    AS time,
       DATE(Event.setDate) AS setDate,
       Event.execPerson_id AS person_id
FROM Event
       INNER JOIN Action Action_amb ON Event.id = Action_amb.event_id
       INNER JOIN ActionType ON Action_amb.actionType_id = ActionType.id
       -- _Action
       INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id
       INNER JOIN ActionProperty AP_queue ON Action_amb.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
       INNER JOIN ActionProperty_Action AP_A_queue ON AP_queue.id = AP_A_queue.id
       -- _Times
       INNER JOIN ActionPropertyType APT_times ON ActionType.id = APT_times.actionType_id
       INNER JOIN ActionProperty AP_times ON Action_amb.id = AP_times.action_id AND APT_times.id = AP_times.type_id
       LEFT JOIN ActionProperty_Time AP_T_times
         ON AP_times.id = AP_T_times.id AND AP_T_times.index = AP_A_queue.index
       -- action and event to delete
       INNER JOIN Action Action_queue ON AP_A_queue.value = Action_queue.id
       INNER JOIN Event Event_queue ON Action_queue.event_id = Event_queue.id
WHERE 1 = 1
  AND Event.deleted = 0
  AND Action_amb.deleted = 0
  AND ActionType.code IN ('amb')
  AND APT_queue.name = 'queue'
  AND APT_times.name = 'times'
  AND AP_A_queue.id = %(queue_id)s
  AND AP_A_queue.index = %(index)s
"""

select_appointments_home = """
SELECT p.id              as person_id,
       p.name            as person_name,
       spec.name         as spec_name,
       APA_queue.id      AS homeActionId,
       APA_queue.index   AS ticketIndex,
       APA_queue.value   as value,
       Client.id         AS client_id,
       Client.lastName   AS client_last_name,
       Client.firstName  AS client_first_name,
       Client.patrName   AS client_patr_name,
       Client.birthDate  AS client_birth_date
FROM Event
       INNER JOIN Action ON Event.id = Action.event_id
       INNER JOIN ActionType ActionType ON Action.actionType_id = ActionType.id
       -- 
       INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id
       INNER JOIN ActionProperty AP_queue ON Action.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
       INNER JOIN ActionProperty_Action APA_queue ON AP_queue.id = APA_queue.id
       --
       INNER JOIN vrbPerson p ON p.id = Event.execPerson_id
       LEFT JOIN rbSpeciality spec ON p.speciality_id = spec.id
       --
       LEFT JOIN Action Action_queue ON APA_queue.value = Action_queue.id
       LEFT JOIN Event Event_queue ON Action_queue.event_id = Event_queue.id
       LEFT JOIN Client ON Event_queue.client_id = Client.id
WHERE Event.deleted = 0
  AND Action.deleted = 0
  AND ActionType.code = 'home'
  AND APT_queue.name = 'queue'
  AND DATE(Event.execDate) = %(search_date)s
"""

select_appointments_home_with_value = (
        select_appointments_home
        + """AND APA_queue.value IS NOT NULL """
)

select_appointments_home_without_value = """
SELECT p.id              as person_id,
       p.lastName        as person_name,
       spec.name         as spec_name,
       APA_queue.id      AS homeActionId,
       APA_queue.index   AS ticketIndex,
       APA_queue.value   as value,
       Client.id         AS client_id,
       Client.lastName   AS client_last_name,
       Client.firstName  AS client_first_name,
       Client.patrName   AS client_patr_name,
       Client.birthDate  AS client_birth_date
FROM Event
       INNER JOIN Action ON Event.id = Action.event_id
       INNER JOIN ActionType ActionType ON Action.actionType_id = ActionType.id
       --
       INNER JOIN ActionPropertyType APT_queue ON ActionType.id = APT_queue.actionType_id
       INNER JOIN ActionProperty AP_queue ON Action.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id
       INNER JOIN ActionProperty_Action APA_queue ON AP_queue.id = APA_queue.id
       --
       INNER JOIN Person p ON p.id = Event.execPerson_id
       LEFT JOIN rbSpeciality spec ON p.speciality_id = spec.id
       --
       LEFT JOIN Action Action_queue ON APA_queue.value = Action_queue.id
       LEFT JOIN Event Event_queue ON Action_queue.event_id = Event_queue.id
       LEFT JOIN Client ON Event_queue.client_id = Client.id
WHERE Event.deleted = 0
  AND Action.deleted = 0
  AND ActionType.code = 'home'
  AND APT_queue.name = 'queue'
  AND DATE(Event.execDate) = %(search_date)s
  AND APA_queue.value IS NULL 
"""

select_req_cnt_home = """
select count(QueueAction.id) as cnt
from Action AS QueueAction
       LEFT JOIN ActionType AS QueueActionType ON QueueActionType.id = QueueAction.actionType_id
       LEFT JOIN Person AS QueuePerson ON QueuePerson.id = QueueAction.person_id
       LEFT JOIN Event AS QueueEvent ON QueueEvent.id = QueueAction.event_id
       LEFT JOIN EventType AS QueueEventType ON QueueEventType.id = QueueEvent.eventType_id
       LEFT JOIN ActionProperty_Action ON ActionProperty_Action.value = QueueAction.id
       LEFT JOIN ActionProperty ON ActionProperty.id = ActionProperty_Action.id
       LEFT JOIN Action ON Action.id = ActionProperty.action_id
       LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
       LEFT JOIN Event ON Event.id = Action.event_id
       LEFT JOIN EventType ON EventType.id = Event.eventType_id
       LEFT JOIN Person ON Person.speciality_id = QueuePerson.speciality_id
where QueueAction.deleted = 0
  AND QueueActionType.code = 'queue'
  AND QueueEvent.deleted = 0
  AND QueueEventType.code = 'queue'
  AND Action.deleted = 0
  AND ActionType.code = 'home'
  AND Event.deleted = 0
  AND EventType.code = '0'
  AND QueueEvent.client_id = %(id_pat)s
  AND Person.id = %(person_id)s
  AND QueueEvent.setDate >= %(set_date)s      
"""

select_max_time_for_overqueue = """
SELECT MAX(AP_T_times.value) as max_time
FROM Event
       INNER JOIN Action A_amb on Event.id = A_amb.event_id
       INNER JOIN ActionType on A_amb.actionType_id = ActionType.id

       INNER JOIN ActionPropertyType APT_queue 
         on ActionType.id = APT_queue.actionType_id 
       INNER JOIN ActionProperty AP_queue 
         on A_amb.id = AP_queue.action_id AND APT_queue.id = AP_queue.type_id

       INNER JOIN ActionPropertyType APT_times 
         on ActionType.id = APT_times.actionType_id 
       INNER JOIN ActionProperty AP_times 
         on A_amb.id = AP_times.action_id AND APT_times.id = AP_times.type_id
       INNER JOIN ActionProperty_Time AP_T_times ON AP_times.id = AP_T_times.id
WHERE 1 = 1
  AND Event.deleted = 0
  AND A_amb.deleted = 0
  AND ActionType.code IN ('amb')
  AND APT_queue.name = 'queue'
  AND APT_times.name = 'times'
  AND AP_queue.id = %(action_id)s
  """

select_doctors_with_geo_and_home = """
SELECT p.id,
       CONCAT(p.lastName, ' ',
              p.firstName, ' ', p.patrName) AS `fio`
FROM Person p
       INNER JOIN Geolocation gl on p.id = gl.person_id -- select docs only with geo

       LEFT JOIN Event e on e.setPerson_id = p.id AND e.setDate = %(date)s
       LEFT JOIN EventType et on et.id = e.eventType_id
       INNER JOIN Action a_home on e.id = a_home.event_id -- select docs only with home queue
       LEFT JOIN ActionType at_home on a_home.actionType_id = at_home.id AND at_home.code = 'home'
WHERE DATE(gl.createDate) = %(date)s
GROUP BY p.id;
"""

select_doctors_home = """
SELECT p.id                                                  AS `id`,
       CONCAT(p.lastName, ' ', p.firstName, ' ', p.patrName) AS `fio`,
       -- rS.id AS `specId`,
       -- rS.name AS `specName`,
       G.lat,
       G.`long`,
       DATE_FORMAT(G.createDate, '%%Y-%%m-%%d')              as createDate
FROM Person p
       INNER JOIN rbSpeciality rS on p.speciality_id = rS.id AND NOT ISNULL(rS.id)

       INNER JOIN Event e ON e.setPerson_id = p.id AND e.deleted = 0
       INNER JOIN EventType et ON et.id = e.eventType_id AND et.code = '0'
       INNER JOIN Action a ON a.event_id = e.id AND a.deleted = 0
       INNER JOIN ActionType apt ON a.actionType_id = apt.id AND apt.code = 'home'

       LEFT JOIN ActionProperty AP_queue on a.id = AP_queue.action_id AND AP_queue.action_id = a.id
       INNER JOIN ActionPropertyType APT_queue on AP_queue.type_id = APT_queue.id AND APT_queue.name = 'queue'

       LEFT JOIN (SELECT g1.lat, g1.`long`, g1.person_id, g1.id, g1.createDate
                  FROM Geolocation g1
                         INNER JOIN (SELECT max(id) max_id, person_id -- join with itself to get last person point id
                                     FROM Geolocation
                                     GROUP BY person_id) g2 ON -- and then get value for this point
                    g1.person_id = g2.person_id AND g1.id = g2.max_id) G on p.id = G.person_id
WHERE 1 = 1
  AND e.setDate >= %(search_from)s
  AND e.setDate <= %(search_to)s

  AND (p.lastAccessibleTimelineDate IS NULL OR p.lastAccessibleTimelineDate = '0000-00-00' OR
       DATE(e.setDate) <= p.lastAccessibleTimelineDate)
  AND (p.timelineAccessibleDays IS NULL OR p.timelineAccessibleDays <= 0 OR
       DATE(e.setDate) <= ADDDATE(CURRENT_DATE(), p.timelineAccessibleDays))
  AND e.id NOT IN (SELECT Event.id
                   FROM Event
                          LEFT JOIN Action ON Action.event_id = Event.id
                          INNER JOIN ActionProperty ON ActionProperty.action_id = Action.id
                          INNER JOIN ActionProperty_rbReasonOfAbsence
                                     ON ActionProperty_rbReasonOfAbsence.id = ActionProperty.id
                          LEFT JOIN ActionType ON ActionType.id = Action.actionType_id
                   WHERE ActionType.code = 'timeLine') -- remove Events in which there is a reason for the absence
  AND p.lastName NOT LIKE 'тест%%'                      -- lock test docs
  AND p.lastName NOT LIKE 'test%%'                      -- lock test docs
GROUP BY fio;
"""
