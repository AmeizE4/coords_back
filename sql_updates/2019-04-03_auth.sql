-- Таблицы и апдейты, необходимые для учета сессий и регистрации

USE s11;
CREATE TABLE IF NOT EXISTS User(
  id INT AUTO_INCREMENT NOT NULL,
  login VARCHAR(128) NOT NULL COMMENT 'Логин',
  password BINARY(60) NOT NULL COMMENT 'Хеш пароля',
  email VARCHAR(128) COMMENT 'Емейл, использующийся для сброса пароля',
  createDatetime DATETIME,
  PRIMARY KEY (id)
) COMMENT 'Используется для авторизации и разграничения прав';


ALTER TABLE Person ADD COLUMN
  user_id INT COMMENT 'Профиль авторизации';
ALTER TABLE Person ADD CONSTRAINT fk_user_id
  FOREIGN KEY (user_id) REFERENCES User(id);


CREATE SCHEMA backend_logger;
USE backend_logger;
CREATE TABLE IF NOT EXISTS Session(
    id INT AUTO_INCREMENT NOT NULL,
    CreateDatetime DATETIME,
    user_id INT NOT NULL COMMENT 'Текущий юзер сессии',
    person_id INT COMMENT 'Текущий профиль юзера в сессии',
    token CHAR(256) 'Токен авторизации',
    expireDatetime DATETIME 'Время, когда токен перестанет быть валидным',
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES s11.User(id),
    FOREIGN KEY (person_id) REFERENCES s11.Person(id)
) COMMENT 'Сессии пользователей. Используется для авторизации.';
